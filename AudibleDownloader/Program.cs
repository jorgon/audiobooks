﻿using AudibleDownloader;
using AudioBooks.Shared;
using Bytewizer.Backblaze.Models;
using Newtonsoft.Json;
using System.Security.Cryptography;
using FileInfo = System.IO.FileInfo;

var cookiefilename = "Cookie.txt";
var tokenfilename = "Token.txt";
var hostfilename = "Host.txt";

if(!File.Exists(cookiefilename))
{
    Console.WriteLine("Cookie not found");
}

Console.WriteLine("Do you wish to update cookie? [enter to skip]");

var cookie = Console.ReadLine();
if (!string.IsNullOrWhiteSpace(cookie))
{
    File.WriteAllText(cookiefilename, cookie);
}
else
{
     cookie = File.ReadAllText("Cookie.txt");
}

string host = Utils.GetSetting(hostfilename, "Audiobook Server");
string token = Utils.GetSetting(tokenfilename, null);


var server = new AudioBooks.Shared.ServerData(host, token, u =>
{
    File.WriteAllText(tokenfilename, u);
}, new DataJson());

if (string.IsNullOrWhiteSpace(token))
{
    var username = Utils.Prompt("Username");
    var password = Utils.Prompt("Password");
    token = await server.AddUser(username, password);

    //token = await server.Auth(username, password);
}

await server.Sync();

//await server.UpdateBooks(server.Books.ToArray());

var src = "Z:\\Audiobooks\\.meta";
var downloader = new Downloader(server, cookie);
AudioBooks.Shared.Book.DestinationPath = "temp";


//var count = await downloader.GetLibrarySize("audiobooks");


/*
HashSet<string> loadedseries = new HashSet<string>();

foreach (var page in Enumerable.Range(1, count))
{
    var pagecontent = await downloader.GetPage(page);

    var books = AudioBooks.Shared.Book.ParseBooks(pagecontent, server).ToArray();
    await server.Update(books);
    
    foreach (var book in books)
    {
        foreach(var seriesid in book.SeriesIDs.ToArray())
        {
            if(!loadedseries.Contains(seriesid))
            {
                await downloader.LoadSeries(seriesid);
                loadedseries.Add(seriesid);
            }
        }
    }
    
}*/

var settings = new BackblazeStorageSettings();

//var endpoint = "s3.us-west-002.backblazeb2.com";
var keyid = settings.KeyId;
var appKey = settings.AppKey;

var client = new Bytewizer.Backblaze.Client.BackblazeClient();
client.Connect(keyid, appKey);

var response = await client.Files.ListAllFilesAsync(settings.Bucket);
var files = response.ToDictionary(f => f.FileName);





var unfinishedresponse = await client.Files.ListUnfinishedAsync(settings.Bucket);

var unfinished = new Dictionary<string, (string FileId, List<int> Parts, List<string> checksums)>();

foreach (var f in unfinishedresponse.Response.Files)
{
    var partsresponse = await client.Parts.ListAsync(f.FileId);


    var numbers = partsresponse.Response.Parts.Select(p => p.PartNumber).ToList();
    var checksums = partsresponse.Response.Parts.Select(p => p.ContentSha1).ToList();

    unfinished[f.FileName] = (f.FileId, numbers, checksums);
}


foreach (var book in server.Books)
{
    var filename = book.ID + ".m4b";
    var path = "Z:\\Audiobooks\\Server\\" + filename;

    try
    {
        if(files.ContainsKey(filename))
        {
            book.ServerStatus |= ServerStatus.Downloaded;
        }
        else if (File.Exists(path) &&  !files.ContainsKey(filename))
        {
            using (var stream = File.OpenRead(path))
            {
                Console.WriteLine($"Uploading {book.Title} ({new FileInfo("Z:\\Audiobooks\\Server\\" + filename).Length})");
                var buffer = new byte[5000000];

                if (buffer.Length >= stream.Length)
                {
                    await client.UploadAsync(settings.Bucket, filename, stream);
                }
                else
                {
                    List<int> parts = new List<int>();
                    List<string> sha1 = new List<string>();
                    string fileid = null;
                    if (!unfinished.ContainsKey(filename))
                    {
                        var meta = await client.Parts.StartLargeFileAsync(settings.Bucket, book.ID + ".m4b");
                        fileid = meta.Response.FileId;
                    }
                    else
                    {
                        fileid = unfinished[filename].FileId;
                        parts = unfinished[filename].Parts;
                        sha1.AddRange(unfinished[filename].checksums);
                    }


                    var url = await client.Parts.GetUploadUrlAsync(fileid);

                    var progress = new Progress();


                    int i = 1;

                    while (stream.Position < stream.Length)
                    {
                        int length = stream.Read(buffer, 0, buffer.Length);

                        // var hash = Convert.ToHexString(SHA1.HashData(buffer));
                        // sha1.Add(hash);

                        if (!parts.Contains(i))
                        {
                            Console.WriteLine($"{book.Title} ( {Math.Round(stream.Position / 1048576f, 2)} / {Math.Round(stream.Length / 1048576f, 2)}) {Math.Round(Math.Round(stream.Position / (double)stream.Length, 2) * 100)}%");

                            MemoryStream part = new MemoryStream(buffer, 0, length, false, false);

                            int failcount = 0;

                            while (failcount < 10)
                            {
                                try
                                {
                                    var result = await client.Parts.UploadAsync(url.Response.UploadUrl, i, url.Response.AuthorizationToken, part, progress);
                                    sha1.Add(result.Response.ContentSha1);

                                    break;
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine(ex.Message);
                                    failcount++;
                                    System.Threading.Thread.Sleep(10000);
                                }
                            }

                        }
                        i++;
                    }

                    var uploadresponse = await client.Parts.FinishLargeFileAsync(fileid, sha1);
                }

            }
            //await server.UploadBookContent(book, path);
            book.ServerStatus |= ServerStatus.Downloaded;
            await server.Update(book);
        }
    }
    catch(Exception e)
    {
        Console.Error.WriteLine(e.ToString());
    }
}

await server.Update(server.Books);
/*
var tempfiles = Directory.GetFiles(src, "*.txt", SearchOption.AllDirectories);
int bookidx = 0;
foreach (var file in tempfiles)
{
    bookidx++;
    if (file.EndsWith(".txt"))
    {
        var l = JsonConvert.DeserializeObject<AudioBooks.Shared.License>(File.ReadAllText(file));
        var id = l.content_license.asin;

        if (serverbooks.ContainsKey(id))
        {
            var book = serverbooks[id];
            if (!book.IsReady)
            {
                var srcpath = Path.ChangeExtension(file, ".aac");
                if (!File.Exists(book.GetInternalFileName("aac")))
                {
                    File.Copy(srcpath, book.GetInternalFileName("aac"));
                }
                await downloader.ConvertBook(book, l);
                Console.WriteLine($"Uploading {bookidx} / {tempfiles.Length} : {book.Title}");
                await server.UploadBookContent(book, book.GetInternalFileName("m4b"));
                book.IsReady = true;
            }
        }
        else
        {
            Console.WriteLine($"Unknown book {file}");
        }
    }
}
*/



//var count = await downloader.GetLibrarySize("audiobooks");

/*
foreach (var page in Enumerable.Range(1, count))
{
    var pagecontent = await downloader.GetPage(page);

    var books = AudioBooks.Shared.Book.ParseBooks(pagecontent, server).ToArray();

    

    foreach(var book in await server.UpdateBooks(books))
    {
        var id = book.ID;
        try
        {
            if (!book.ServerStatus.HasFlag(AudioBooks.Shared.ServerStatus.Downloaded))
            {
                if(files.ContainsKey(book.ID + ".m4b"))
                {
                    book.ServerStatus |= ServerStatus.Downloaded;
                    await server.Update(book);
                }
                else if (book.Title != "The New York Times Digest")
                {
                    Console.WriteLine($"Processing {book.Title} {id}");
                    book.ToString();
                    var license = await downloader.DownloadBook(book);
                    await downloader.ConvertBook(book, license);

                    var destfilename = book.GetInternalFileName("m4b");

                    await server.UploadBookContent(book, destfilename);
                }
            }
        }
        catch(Exception ex)
        {
            Console.WriteLine(ex.ToString()); 
        }
    }


}

*/

class Progress : IProgress<Bytewizer.Backblaze.Models.ICopyProgress>
{
    public void Report(Bytewizer.Backblaze.Models.ICopyProgress value)
    {

    }
}
