﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AudibleDownloader
{
    internal static class Utils
    {
        public static string Prompt(string prompt)
        {
            Console.Write(prompt + ": ");
            return Console.ReadLine();
        }

        public static string GetSetting(string filename, string prompt)
        {
            if (File.Exists(filename))
            {
                return File.ReadAllText(filename);
            }
            if (prompt == null)
                return null;

            var val = Prompt(prompt);
            File.WriteAllText(filename, val);
            return val;
        }
    }
}
