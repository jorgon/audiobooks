﻿using Newtonsoft.Json;
using System.Text.RegularExpressions;
using System.Net.Http.Headers;
using System.Text;
using System.Net;
using System.Diagnostics;
using AudioBooks.Shared;

namespace AudibleDownloader
{
    public class Downloader
    {
        private static string useragent =
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36";

        string Cookie;
        HttpClient client;
        CookieContainer cookieContainer;
        IData Data;
        public Downloader(IData data, string cookie)
        {
            Data = data;
            Cookie = cookie;
            var handler = new HttpClientHandler();
            handler.CookieContainer = cookieContainer = new CookieContainer();

            LoadCookies();

            client = new HttpClient(handler);
        }

        private void LoadCookies()
        {
            cookieContainer.PerDomainCapacity = 100;
            var cookie = Cookie;
            var cookies = cookie.Split(';').Select(c => c.Split('=', 2)).Where(c => c.Length == 2).Select(c => new { name = c[0], value = c[1] });
            //cookieContainer.SetCookies(new Uri("https://audible.com"), cookie);
            foreach (var c in cookies)
            {
                try
                {
                    AddCookie(c.name, c.value);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);

                }

            }
        }

        void AddCookie(string name, string value)
        {
            cookieContainer.Add(new Uri("https://audible.com"), new Cookie(name.Trim(), value.Trim()));
            cookieContainer.Add(new Uri("https://www.audible.com"), new Cookie(name.Trim(), value.Trim()));
            cookieContainer.Add(new Uri("https://api.audible.com"), new Cookie(name.Trim(), value.Trim()));
        }

        public async Task LoadSeries(string seriesid)
        {
            var result = Data.Series.FirstOrDefault(s => s.ID == seriesid);
            if(result == null)
            {
                result = new Series()
                {
                    ID = seriesid
                };
                
            }

            var url = "https://www.audible.com/series/" + seriesid;
            result.Books.Clear();
List<Book> books = new List<Book>();
            while (url != null)
            {

                var page = await Get(url);

                var titlematch = Regex.Match(page, "<meta property=\"og:title\" content=\"(?<title>.*?)\"", RegexOptions.Singleline);
                //var summarymatch = Regex.Match(page, "series-summary-content(.*?)>");
                var summarymatch = Regex.Match(page, "series-summary-content(.*?)>(?<summary>.*?)</div>", RegexOptions.Singleline);
                var nextpagematch = Regex.Match(page, "nextButton refinementFormButton(.*?)href=\"(?<nextpage>.*?)\"", RegexOptions.Singleline);
                result.Name = titlematch.Groups["title"].Value;
                result.Summary = summarymatch.Groups["summary"].Value;

                result.Summary =
                    result.Summary.CleanHtml();

                
                foreach (var _book in Book.ParseSeriesBooks(page, Data))
                {
                    var book = Data.GetBook(_book.ID);
                    if (book == null)
                        book = _book;

                    book.Update(_book);


                    if (!book.SeriesIDs.Contains(seriesid))
                    {
                        book.SeriesIDs.Add(seriesid);
                    }

                    books.Add(book);
                    result.Books.Add(book.ID);
                }
                if (nextpagematch.Success)
                {
                    url = "https://www.audible.com" + nextpagematch.Groups[2].Value;
                }
                else
                {
                    url = null;
                }


            }

            
            
            await Data.Update(result);
            await Data.Update(books);

        }

        public async Task<int> GetLibrarySize(string type)
        {
            var src = await Get("https://www.audible.com/library/" + type);

            List<int> pages = new List<int>();

            foreach (Match segment in Regex.Matches(src, "/library/"+ type + "\\?page=(.*?)&", RegexOptions.Singleline))
            {
                pages.Add(int.Parse(segment.Groups[1].Value));
            }
            return pages.Max();
        }



        public async Task<string> GetPage(int pageno, bool podcast = false)
        {
            var url =
                $"https://www.audible.com/library/audiobooks?piltersCurrentValue=All&sortBy=PURCHASE_DATE.dsc&page={pageno}";

            if (podcast)
                url = $"https://www.audible.com/library/podcasts?piltersCurrentValue=All&sortBy=PURCHASE_DATE.dsc&page={pageno}";
            return await Get(url);
        }

        private async Task<string> Get(string url)
        {
            var message = new HttpRequestMessage(HttpMethod.Get,
                            url);

            AddHeaders(message.Headers, @"sec-fetch-dest: document
sec-fetch-mode: navigate
sec-fetch-site: same-origin
sec-fetch-user: ?1
upgrade-insecure-requests: 1
user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36");

            var response = await client.SendAsync(message);
            UpdateCookies(response);
            var content = await response.Content.ReadAsStringAsync();
            return content;
        }

        
        

        public async Task<License> DownloadBook(Book book)
        {
            /*if (File.Exists(book.GetOverrideFilename()))
            {
                book = JsonConvert.DeserializeObject<Book>(File.ReadAllText(book.GetOverrideFilename()));
            }*/
            var dest = book.GetInternalFileName("aac");
            
            var jsonfile = book.GetInternalFileName("txt");
            
                try
                {
                    var player = await GetWebPlayer(book.ID);
                    var data = await GetLicense(book.ID);
                    var contentlicense = await GetContentLicenseAjax(book.ID, player["token"]);

                if (File.Exists(dest))
                    return data;
                    
                    if (data.content_license != null)
                    {
                        var playlist = await GetPlaylist(data);

                        var url = new Uri(data.content_license.license_response);

                        var metadatafolder =
                            Path.GetDirectoryName(book.GetInternalFileName("tmp"));

                        //if (!Directory.Exists(metadatafolder))
                        {
                            Directory.CreateDirectory(metadatafolder);
                        }

                        var src =
                            $"{url.Scheme}://{url.Host}{string.Join("", url.Segments.Take(url.Segments.Length - 1))}";

                    
                        Console.WriteLine($"{book.GetAuthorNames(Data)} {book.Title}");
                        await DownloadPlaylist(book, playlist, src, dest);
                        Console.WriteLine();
                        File.WriteAllText(jsonfile, JsonConvert.SerializeObject(data));
                    return data;
                    }
                }
                catch (Exception e)
                {
                    if (!File.Exists(book.GetOverrideFilename()))
                    {
                        File.WriteAllText(book.GetOverrideFilename(), JsonConvert.SerializeObject(book));
                    }
                    Console.WriteLine(e);
                    throw;
                }

            throw new Exception("Failed to get content license");
        }

        private async Task<PlaylistItem[]> GetPlaylist(License data)
        {
            HttpRequestMessage message;
            HttpResponseMessage result;
            message = new HttpRequestMessage(HttpMethod.Get, data.content_license.license_response);

            AddHeaders(message.Headers, @"Accept: * /*
Accept-Language: en-US,en;q=0.9
Cache-Control: no-cache
Connection: keep-alive
DNT: 1
Host: d1jobzhhm62zby.cloudfront.net
Origin: https://www.audible.com
Pragma: no-cache
Referer: https://www.audible.com/
Sec-Fetch-Dest: empty
Sec-Fetch-Mode: cors
Sec-Fetch-Site: cross-site
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36
");


            result = await client.SendAsync(message);
            UpdateCookies(result);
            var m3u = await result.Content.ReadAsStringAsync();

            var ret = new List<PlaylistItem>();
            var item = new PlaylistItem();
            foreach (var line in m3u.Split("\n"))
            {

                if (line.StartsWith("#EXT-X-BYTERANGE"))
                {
                    item.ParseRange(line);
                }

                if (!line.StartsWith("#"))
                {
                    item.path = line;
                    ret.Add(item);
                    item = new PlaylistItem();
                }
            }

            return ret.ToArray();
        }

        public async Task<Dictionary<string, string>> GetWebPlayer(string asin)
        {
            var url = $"https://www.audible.com/webplayer?asin={asin}&contentDeliveryType=MultiPartBook&ref_=a_libraryItem_cloudplayer_{asin}&fetchNewPlayQueue=true&overrideLph=false&initialCPLaunch=true";
            
            var message = new HttpRequestMessage(HttpMethod.Get, url);
            //message.Headers.Add("Cookie", cookie);

            AddHeaders(message.Headers, @"Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,* /*;q=0.8,application/signed-exchange;v=b3;q=0.9
Accept-Language: en-US,en;q=0.9
Cache-Control: no-cache
Connection: keep-alive
DNT: 1
Host: www.audible.com
Pragma: no-cache
Referer: https://www.audible.com/
Sec-Fetch-Dest: document
Sec-Fetch-Mode: navigate
Sec-Fetch-Site: same-origin
Sec-Fetch-User: ?1
Upgrade-Insecure-Requests: 1");

            var response = await client.SendAsync(message);
            UpdateCookies(response);

            var contents = await response.Content.ReadAsStringAsync();

            var matches = Regex.Matches(contents, "<input(.*)>");

            var pairs = matches.Select(m => m.ToString()).Select(match => new KeyValuePair<string, string>(
                Regex.Match(match, "name=\"(.*)\" ").ToString().Split('=', 2).Last().Trim('"', ' '),
                Regex.Match(match, "value=\"(.*)\">").ToString().Split('=', 2).Last().Trim('"', ' ', '>')));
            var ret = new Dictionary<string, string>();

            foreach (var pair in pairs)
            {
                ret[pair.Key] = pair.Value;
            }

            return ret;
        }

        private void AddHeaders(HttpRequestHeaders headers, string lines)
        {
            foreach (var line in lines.Split("\n").Select(l => l.Trim()).Where(l => l.Length > 0))
            {
                var parts = line.Split(':', 2);
                headers.Add(parts[0].Trim(), parts[1].Trim());
            }
        }

        private async Task<License> GetLicense(string asin)
        {
            var message = new HttpRequestMessage(HttpMethod.Post, $"https://api.audible.com/1.0/content/{asin}/licenserequest");

            message.Headers.Add("Host", "api.audible.com");

            message.Content = new StringContent(
                "{\"response_groups\":\"chapter_info,content_reference,last_position_heard\",\"chapter_titles_type\":\"Tree\",\"supported_drm_types\":[\"Hls\",\"Mpeg\"],\"use_adaptive_bit_rate\":false,\"consumption_type\":\"Streaming\"}",
                Encoding.ASCII,
                "application/json");

            AddHeaders(message.Headers, @"sec-ch-ua: "" Not A;Brand"";v=""99"", ""Chromium"";v=""100"", ""Google Chrome"";v=""100""
pragma: no-cache
DNT: 1
sec-ch-ua-mobile: ?0
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36
Accept: application/json
cache-control: no-cache
Client-ID: WebPlayerApplication
sec-ch-ua-platform: ""Windows""
Origin: https://www.audible.com
Sec-Fetch-Site: same-site
Sec-Fetch-Mode: cors
Sec-Fetch-Dest: empty
Referer: https://www.audible.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9");
            /*
            message.Headers.Add("Accept", "application/json");

            message.Headers.Add("pragma", "no-cache");
            message.Headers.Add("cache-control", "no-cache");
            message.Headers.Add("DNT", "1");
            message.Headers.Add("Client-ID", "WebPlayerApplication");
            message.Headers.Add("UserAgent",
                useragent);
            message.Headers.Add("Origin", "https://www.audible.com");
            message.Headers.Add("Sec-Fetch-Dest", "empty");
            message.Headers.Add("Sec-Fetch-Mode", "cors");
            message.Headers.Add("Sec-Fetch-Site", "same-site");
            message.Headers.Add("Referer", "https://www.audible.com/");

            message.Headers.Add("Accept-Language", "en-US,en;q=0.9");*/

            
            //message.Headers.Add("Cookie", Cookie);


            var result = await client.SendAsync(message);
            UpdateCookies(result);

            var content = await result.Content.ReadAsStringAsync();

            var data = JsonConvert.DeserializeObject<License>(content);
            return data;
        }

        private async Task<string> GetContentLicenseAjax(string asin, string token)
        {
            var message =
                new HttpRequestMessage(HttpMethod.Post, "https://www.audible.com/webplayer/contentlicenseajax");
            AddHeaders(message.Headers, $@"
Accept: application/json, text/javascript, * /*; q=0.01
Accept-Language: en-US,en;q=0.9
Cache-Control: no-cache
Connection: keep-alive
DNT: 1
Host: www.audible.com
Origin: https://www.audible.com
Pragma: no-cache
Referer: https://www.audible.com/webplayer?asin={asin}&contentDeliveryType=PodcastEpisode&ref_=a_minerva_cloudplayer_B08L4XZHXV&cloudPlayerStartLoadTime=1615217294985&fetchNewPlayQueue=true&overrideLph=false
Sec-Fetch-Dest: empty
Sec-Fetch-Mode: cors
Sec-Fetch-Site: same-origin
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36
X-Requested-With: XMLHttpRequest
");
            var form = new List<KeyValuePair<string, string>>();
            form.Add(new KeyValuePair<string, string>("asin", asin));
            form.Add(new KeyValuePair<string, string>("token", token));
            form.Add(new KeyValuePair<string, string>("key", "AudibleCloudPlayer"));
            form.Add(new KeyValuePair<string, string>("action", "getPlayQueue"));

            message.Content = new FormUrlEncodedContent(form);

            var response = await client.SendAsync(message);
            UpdateCookies(response);
            var content = await response.Content.ReadAsStringAsync();

            return "";
        }


        private async Task DownloadPlaylist(Book book, PlaylistItem[] playlist, string rootpath, string destination)
        {
           
            var str = File.Create(destination);
            int part = 1;
            foreach (var item in playlist)
            {
                if (item.length > 0)
                {
                    Console.Write($"\r{part} / {playlist.Length}");
                    
                    //Data.Get().UpdateBook(book);
                    var itemstr = await DownloadItem(rootpath, item);
                    itemstr.CopyTo(str);
                    itemstr.Close();
                    part++;
                }
            }
            str.Close();


            
            await Data.Update(book);

        }

        private async Task<Stream> DownloadItem(string rootpath, PlaylistItem item)
        {
            var url = new Uri(rootpath + item.path);
            var message =
                new HttpRequestMessage(HttpMethod.Get, url.OriginalString);
            AddHeaders(message.Headers, $@"Accept: * /*
Accept-Encoding: identity
Accept-Language: en-US,en;q=0.9
Cache-Control: no-cache
Connection: keep-alive
DNT: 1
Host: {url.Host}
Origin: https://www.audible.com
Pragma: no-cache
Range: bytes={item.start}-{item.length + item.start - 1}
Referer: https://www.audible.com/
Sec-Fetch-Dest: empty
Sec-Fetch-Mode: cors
Sec-Fetch-Site: cross-site
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36");


            var response = await client.SendAsync(message);

            UpdateCookies(response);

            var content = await response.Content.ReadAsStreamAsync();
            return content;
        }

        private void UpdateCookies(HttpResponseMessage response)
        {
            foreach(var header in response.Headers.Where(h => string.Equals(h.Key, "Set-Cookie", StringComparison.InvariantCultureIgnoreCase)))
            {
                foreach (var value in header.Value)
                {
                    var cookie = value.Split('=', ';');
                    var name = cookie[0];
                    var cvalue = cookie[1];

                    AddCookie(name, cvalue);
                }
            }
        }

        public async Task ConvertBook(Book book, License license)
        {
            if (!File.Exists(book.GetInternalFileName("m4b")))
            {
                var metadatafile = CreateMetadataFile(book, license);
                RunFFMpeg(book, metadatafile);

                var img = book.GetInternalFileName("jpg");
                if (!File.Exists(img))
                {
                    img = await DownloadImage(book, license);
                    
                }
                AddCover(book, img);
            }

            

            book.ServerStatus = ServerStatus.Downloaded;

            await Data.Update(book);
        }

        private static void AddCover(Book book, string img)
        {
            var info = new ProcessStartInfo("C:\\Users\\Paul\\Apps\\AtomicParsley.exe",
                $"\"{book.GetInternalFileName("m4b")}\" --artwork \"{img}\" --overWrite");



            Console.WriteLine($"Adding Cover Art {book.Title}");

            //info.UseShellExecute = false;
            //info.RedirectStandardOutput = true;
            //info.RedirectStandardError = true;
            //info.RedirectStandardInput = true;

            //info.CreateNoWindow = true;
            //info.WindowStyle = ProcessWindowStyle.Hidden;


            var process = Process.Start(info);
            int i = 0;
            process.OutputDataReceived += (sender, args) => i++;
            //process.BeginOutputReadLine();

            process.WaitForExit();
        }

        private async Task<string> DownloadImage(Book book, License license)
        {
            var str = await client.GetStreamAsync(book.ImageUrl);

            var filename = book.GetInternalFileName(book.ImageUrl.Split('.').Last());

            var dest = File.Create(filename);
            await str.CopyToAsync(dest);
            dest.Close();
            str.Close();
            return filename;
        }

        private static void RunFFMpeg(Book book, string metadatafile)
        {
            var dest = book.GetInternalFileName("m4b");
            var dir = Directory.GetParent(dest);
            Directory.CreateDirectory(dir.FullName);
            var info = new ProcessStartInfo("C:\\Users\\Paul\\Apps\\ffmpeg.exe",
                $"-i \"{book.GetInternalFileName("aac")}\" -i \"{metadatafile}\" -map_metadata 1 -codec copy \"{dest}\"");



            Console.WriteLine($"Converting {dest}");

            //info.UseShellExecute = false;
            //info.RedirectStandardOutput = true;
            //info.RedirectStandardError = true;
            //info.RedirectStandardInput = true;

            //info.CreateNoWindow = true;
            //info.WindowStyle = ProcessWindowStyle.Hidden;

            var process = Process.Start(info);
            int i = 0;
            // process.OutputDataReceived += (sender, args) => i++;
            //process.BeginOutputReadLine();

            process.WaitForExit();
        }

        private string CreateMetadataFile(Book book, License meta)
        {
            var outfile = book.GetInternalFileName("meta");

            var ret = new StringBuilder();

            ret.AppendLine($@";FFMETADATA1
title={book.Title}
artist={book.GetAuthorNames(Data)}
");

            foreach (var chap in meta.content_license.content_metadata.chapter_info.chapters)
            {
                ret.AppendLine($@"[CHAPTER]
TIMEBASE=1/1000
START={chap.start_offset_ms}
END={chap.start_offset_ms + chap.length_ms}
title={chap.title}
");

            }

            File.WriteAllText(outfile, ret.ToString());
            return outfile;
        }


    }
}
