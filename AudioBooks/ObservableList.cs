﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AudioBooks
{
    public class ObservableList<T> : IList<T>, INotifyCollectionChanged, INotifyPropertyChanged
    {
        List<T> list = new List<T>();

        public T this[int index] { get => ((IList<T>)list)[index]; set  {
                ((IList<T>)list)[index] = value;
                Notify(NotifyCollectionChangedAction.Replace);
            }
        }

        public int Count => ((ICollection<T>)list).Count;

        public bool IsReadOnly => ((ICollection<T>)list).IsReadOnly;

        public event NotifyCollectionChangedEventHandler CollectionChanged;
        public event PropertyChangedEventHandler PropertyChanged;

        void Notify(NotifyCollectionChangedAction action)
        {
            CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        public void Add(T item)
        {
            ((ICollection<T>)list).Add(item);
            Notify(NotifyCollectionChangedAction.Add);
        }

        public void AddRange(IEnumerable<T> items)
        {
            list.AddRange(items);
            Notify(NotifyCollectionChangedAction.Add);
        }

        public void Clear()
        {
            ((ICollection<T>)list).Clear();
            Notify(NotifyCollectionChangedAction.Reset);
        }

        public bool Contains(T item)
        {
            return ((ICollection<T>)list).Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            ((ICollection<T>)list).CopyTo(array, arrayIndex);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return ((IEnumerable<T>)list).GetEnumerator();
        }

        public int IndexOf(T item)
        {
            return ((IList<T>)list).IndexOf(item);
        }

        public void Insert(int index, T item)
        {
            ((IList<T>)list).Insert(index, item);
            Notify(NotifyCollectionChangedAction.Add);
        }

        public bool Remove(T item)
        {
            var ret = ((ICollection<T>)list).Remove(item);
            Notify(NotifyCollectionChangedAction.Remove);
            return ret;
        }

        public void RemoveAt(int index)
        {
            ((IList<T>)list).RemoveAt(index);
            Notify(NotifyCollectionChangedAction.Remove);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)list).GetEnumerator();
        }
    }
}
