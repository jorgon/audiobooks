﻿

namespace AudioBooks
{
    public partial class App : Application
    {
        private readonly ISettings settings;
        private readonly ServerData data;

        public App(ISettings settings, ServerData data)
        {
            InitializeComponent();

            MainPage = new AppShell(settings, data, new ViewModel.AppShellModel(settings));
            this.settings = settings;
            this.data = data;
        }

        protected override void OnStart()
        {
            base.OnStart();
           /* if (!)
            {

               MainPage.Navigation.PushModalAsync(new Pages.Authenticate(data, settings));
            }*/
        }
    }
}