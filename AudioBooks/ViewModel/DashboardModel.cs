﻿using AudioBooks.Shared;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using Microsoft.Maui.Dispatching;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AudioBooks.ViewModel
{
    [ObservableObject]
    public partial class DashboardModel
    {
        [RelayCommand]
        async Task Refresh()
        {
            try
            {
                IsLoading = true;
                await Data.Sync();
                ApplyFilter();
                
            }
            catch(Exception e)
            {

            }
            finally
            {
                IsLoading = false;
            }
        }

        ServerData Data;
        IDispatcher dispatcher;
        ImageCacheService _cache;
        public DashboardModel(ServerData data, ImageCacheService cache)
        {
            _cache = cache;
            Data = data;
            dispatcher = Dispatcher.GetForCurrentThread();
        }

        [ObservableProperty]
        bool isLoading = false;

        [RelayCommand]
        void ApplyFilter()
        {
            if(Data.Books.Count() == 0)
            {
                Task.Run(() => RefreshCommand.Execute(null));
            }
            //dispatcher.Dispatch(new Action(() =>
            {
                
                var newbooks = Data.Books.Where(b => b.ServerStatus.HasFlag(ServerStatus.Downloaded));
                if (Books.Count != newbooks.Count())
                {
                    Books.Clear();
                    

                    Books.AddRange(newbooks.OrderBy(b => b.Title).Select(b => new BookCardModel(b, Data, _cache)));
                }
            }
            //));
        }

        public ObservableList<BookCardModel> _Books = new ();
        public ObservableList<BookCardModel> Books { get
            {
                return _Books;
            }
        }
    }
}
