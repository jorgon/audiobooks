﻿using AudioBooks.Shared;
using CommunityToolkit.Mvvm.ComponentModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AudioBooks.ViewModel
{
    [QueryProperty(nameof(Book), nameof(Book))]
    public partial class BookDetailModel : ObservableObject
    {
        [ObservableProperty]
        [NotifyPropertyChangedFor(nameof(Authors))]
        [NotifyPropertyChangedFor(nameof(Available))]
        [NotifyPropertyChangedFor(nameof(NotDownloaded))]
        [NotifyPropertyChangedFor(nameof(NotInQueue))]
        private Book book;

        
        public ObservableList<SeriesModel> Series { get; } = new ObservableList<SeriesModel>();
        public ObservableList<Book> ByAuthor { get; } = new ObservableList<Book>();

        IData Data;

        PlatformSettings platform;

        public BookDetailModel(ServerData data, PlatformSettings platform)
        {
            Data = data;
            this.platform = platform;
        }

        protected override void OnPropertyChanging(System.ComponentModel.PropertyChangingEventArgs e)
        {
            base.OnPropertyChanging(e);
        }

        protected override void OnPropertyChanged(System.ComponentModel.PropertyChangedEventArgs e)
        {
            if(e.PropertyName == "Book")
            {
                Series.Clear();
                Series.AddRange(this.Book.SeriesIDs.Select(id => new SeriesModel(Data, id)));

                ByAuthor.Clear();
                ByAuthor.AddRange(Data.Books.Where(b => b.AuthorIds.Any(id => this.Book.AuthorIds.Contains(id)) && b.ID != Book.ID));
            }
            base.OnPropertyChanged(e);
        }

        public bool Available => Book == null ? false : Book.ServerStatus.HasFlag(ServerStatus.Downloaded);
        public bool NotDownloaded => Book == null ? true : !Book.IsDownloaded(platform.DocumentDirectory) && Available;
        public bool NotInQueue => true;
        public string Authors => Book?.GetAuthorNames(Data);
        
    }
}
