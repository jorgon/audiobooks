﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AudioBooks.Shared;
using CommunityToolkit.Mvvm.ComponentModel;

namespace AudioBooks.ViewModel
{
    [QueryProperty(nameof(SeriesID), nameof(SeriesID))]
    public partial class SeriesModel : ObservableObject
    {
        public SeriesModel(IData data)
        {
            Data = data;
        }       
        
        public SeriesModel(IData data, string id)
        {
            Data = data;
            SeriesID = id;
        }

        public IData Data { get; }

        [ObservableProperty]
        string seriesID;

        [ObservableProperty]
        Series series;

        public ObservableList<Book> Books { get; } = new();

        protected override void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(SeriesID))
            {
                this.Series = Data.Series.FirstOrDefault(s => s.ID == SeriesID);

                Books.Clear();
                if(this.Series != null)
                    Books.AddRange(Data.GetSeriesBooks(this.Series));
            }
            base.OnPropertyChanged(e);
        }
    }
}
