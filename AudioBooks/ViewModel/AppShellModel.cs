﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AudioBooks.ViewModel
{
    public partial class AppShellModel : ObservableObject
    {
        [ObservableProperty]
        bool _NotLoggedIn;

        public AppShellModel(ISettings settings)
        {
            NotLoggedIn = !(settings.HasToken || string.IsNullOrWhiteSpace(settings.Token));
        }
    }
}
