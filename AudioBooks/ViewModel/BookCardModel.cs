﻿using AudioBooks.Shared;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using Microsoft.Maui.Dispatching;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AudioBooks.ViewModel
{
    [QueryProperty(nameof(Book), nameof(Book))]
    public partial class BookCardModel : ObservableObject
    {
        [ObservableProperty]
        [NotifyCanExecuteChangedFor(nameof(UpdateImageCommand))]
        [NotifyPropertyChangedFor(nameof(Title), nameof(Author), nameof(AvailableOpacity), nameof(NotDownloaded))]
        private Book book;

        [ObservableProperty]
        ImageSource imageUrl = "bookplaceholder.png";
        //ImageSource imageUrl = ImageSource.FromResource("AudioBooks.Resources.Images.bookplaceholder.png");

        ImageCacheService Cache;
        IDispatcher dispatcher;

        IData _data;

        public BookCardModel(IData data, ImageCacheService cache)
        {
            Cache = cache;
            _data = data;
            dispatcher = Dispatcher.GetForCurrentThread();
        }        
        
        public BookCardModel(Book book, IData data, ImageCacheService cache)
        {
            this.book = book;
            
            Cache = cache;
            _data = data;
            dispatcher = Dispatcher.GetForCurrentThread();
            Task.Run(UpdateImage);
        }

        protected override void OnPropertyChanging(System.ComponentModel.PropertyChangingEventArgs e)
        {

            dispatcher.DispatchDelayed(TimeSpan.FromMilliseconds(10), () =>
            {
                base.OnPropertyChanging(e);
            });
        }

        protected override void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            dispatcher.DispatchDelayed(TimeSpan.FromMilliseconds(10), () =>
            {
                base.OnPropertyChanged(e);
            });

        }

        [RelayCommand]
        async Task UpdateImage()
        {
           // ImageUrl = ImageSource.FromResource("AudioBooks.Resources.Images.bookplaceholder.png");
            if (Book?.ImageUrl != null)
            {
                //ImageUrl = Book.ImageUrl;
                System.Threading.ThreadPool.QueueUserWorkItem((o) => Cache.Load(Book.ImageUrl, url => ImageUrl = url));
            }

        }


        public bool NotDownloaded => Book == null ? true: !Book.ServerStatus.HasFlag(ServerStatus.Downloaded);

        //List<SeriesModel> Series => book.SeriesIDs.Select(b => new SeriesModel()).ToList();

		public string Title => Book?.Title ?? string.Empty;

        public string Author => Book?.GetAuthorNames(_data);

        
        public float AvailableOpacity => Book.IsAvailable ? 1.00f : .40f;

        //public string SeriesText => Book?.SeriesText ?? string.Empty;
    }
}
