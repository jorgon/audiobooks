﻿using AudioBooks.Shared;


namespace AudioBooks.Pages
{
	
	public partial class Authenticate : ContentPage
	{
        ServerData Server = null;
        private readonly ISettings settings;

        public Authenticate (ServerData server, ISettings settings)
		{

            InitializeComponent();
            Server = server;
            this.settings = settings;
            Host.Text = settings.Host;
        }

        private void Login_Clicked(object sender, EventArgs e)
        {
            settings.Host = Host.Text.Trim();
            Server.Host = settings.Host;
            Task.Run(async () => {
                var token = await Server.Auth(Username.Text.Trim(), Password.Text.Trim());
                if (!string.IsNullOrWhiteSpace(token.Token))
                {
                    settings.Token = token.Token;
                    await Shell.Current.GoToAsync("//Browse");
                }
                else
                {
                    ErrorMessage.Text = token.Error;
                    ErrorMessage.IsVisible = true;
                }
            });
        }

        private void CreateAccount_Clicked(object sender, EventArgs e)
        {
            settings.Host = Host.Text.Trim();
            Task.Run(async () => {
                settings.Token = await Server.AddUser(Username.Text.Trim(), Password.Text.Trim());
            });
        }
    }
}