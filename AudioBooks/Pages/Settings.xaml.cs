using AudioBooks.Shared;

namespace AudioBooks.Pages;

public partial class Settings : ContentPage
{
    ServerData Server = null;
    private readonly ISettings settings;

    public Settings(ServerData server, ISettings settings)
	{
		InitializeComponent();
        Server = server;
        this.settings = settings;

	}

    private void Login_Clicked(object sender, EventArgs e)
    {
        Shell.Current.GoToAsync(nameof(Pages.Authenticate), new Dictionary<string, object>
        {
        });
    }

    private void ResetSettings_Clicked(object sender, EventArgs e)
    {
        settings.Reset();
    }
}