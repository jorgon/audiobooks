namespace AudioBooks.Pages;

public partial class BookDetail : ContentPage
{

    PlatformSettings platform;
    BookDetailModel VM;
    ServerData server;

    public BookDetail(BookDetailModel vm, PlatformSettings platform, ServerData server)
    {
        InitializeComponent();

        this.BindingContext = VM = vm;
        this.platform = platform;
        this.server = server;
    }

    protected override void OnNavigatedTo(NavigatedToEventArgs args)
	{
		base.OnNavigatedTo(args);
	}

    private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
    {
        var view = (Components.BookCard)sender;

        Shell.Current.GoToAsync(nameof(BookDetail), new Dictionary<string, object>
        {
            ["Book"] = (Book)view.BindingContext
        });
    }

    private void Download_Clicked(object sender, EventArgs e)
    {
        Task.Run(DoDownload);
    }

    async Task DoDownload()
    {
        var dir = platform.DocumentDirectory;

        var filename = VM.Book.GetFileName("");
        try
        {
            string src = await server.GetDownloadUrl(VM.Book);
            platform.Download(src, filename);
        }
        catch(Exception ex)
        {
            ex.ToString();
        }
    }

    private void AddToQueue_Clicked(object sender, EventArgs e)
    {
        server.AddToQueue(server.User, VM.Book.ID);
    }
}