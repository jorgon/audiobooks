﻿using Microsoft.Maui.Layouts;
using CommunityToolkit.Mvvm.ComponentModel;
using Maui.BindableProperty.Generator.Core;

namespace AudioBooks.Pages.Components
{
    public partial class BetterGrid : Microsoft.Maui.Controls.Layout, ILayoutManager
    {
        [AutoBindable]
        double _spacing;

        ScrollView _scroll = null;

        protected override void OnParentChanged()
        {
            base.OnParentChanged();

            UpdateScroll();
        }

        void UpdateScroll()
        {
            if (_scroll != null)
            {
                _scroll.Scrolled -= _scroll_Scrolled;
            }

            var parent = Parent;
            while (parent != null)
            {
                if (parent is ScrollView sv)
                    _scroll = sv;
                parent = parent.Parent;
            }

            if (_scroll != null)
            {
                _scroll.Scrolled += _scroll_Scrolled;
            }
        }

        private void _scroll_Scrolled(object sender, ScrolledEventArgs e)
        {

            //var content = _scroll.ContentSize;
            //var computed = _scroll.ComputeDesiredSize(_scroll.wi);
            /*var bounds = _scroll.GetPosition(_scroll);
            this.X*/

            UpdateScroll();
            
            ArrangeChildren(Bounds);
        }

        bool InArrange = false;
        Rect lastArrange = Rect.Zero;
        public Size ArrangeChildren(Rect bounds)
        {
            if (InArrange)
                return Size.Zero;
            InArrange = true;

            

            var changed = lastArrange != bounds;
            try
            {
                if (_scroll == null)
                {
                    UpdateScroll();
                }

            var globaly = 0.0;
            Element p = this;
            while (p != _scroll)
            {
                if (p is VisualElement v)
                {
                    globaly += v.Y;
                }
                p = p.Parent;
            }
                if (Children.Count == 0)
                    return new Size(0, 0);

                var padding = Spacing;

                var minwidth = Children.Max(c => c.MinimumWidth);
                var maxwidth = Children.Min(c => c.MaximumWidth);

                //            var minheight = Children.Max(c => c.MinimumHeight);
                //            var maxheight = Children.Min(c => c.MaximumHeight);

                var targetwidth = (minwidth + maxwidth) / 2;
                //var targetheight = (minheight + maxheight) / 2;

                if (double.IsNaN(targetwidth))
                    targetwidth = 300;
                //if (double.IsNaN(targetheight))
                //    targetheight = 300;

                var columncount = (int)Math.Floor((bounds.Width - padding) / (targetwidth + padding));
                if (columncount == 0)
                    columncount = 1;

                if (columncount == 1)
                {
                    targetwidth = ((bounds.Width - padding) / 3) - padding;
                    columncount = 3;
                }

                var rowcount = (int)Math.Ceiling(Children.Count / (decimal)columncount);

                var rowheights = new double[rowcount];

                for (var i = 0; i < rowcount; i++)
                {
                    rowheights[i] = Children.Skip(i * columncount).Take(columncount).Max(c => c.Measure(targetwidth, double.MaxValue).Height);
                }

                Rect? visible = null;

                if (_scroll != null)
                {
                    var desired = _scroll.DesiredSize;
                    var content = this.DesiredSize;

                    var curbounds = new Rect(0, globaly, content.Width, content.Height);
                    var scrollbounds = _scroll.Bounds;
                    Rect scrollwindow = new Rect(_scroll.ScrollX + scrollbounds.X, _scroll.ScrollY + scrollbounds.Y, desired.Width, desired.Height);

                    visible = curbounds.Intersect(scrollwindow);
                   
                }

                var outerpadding = (bounds.Width - ((columncount * targetwidth) + Math.Max((columncount - 1) * padding, 0))) / 2;

                int idx = 0;

                foreach (var child in Children)
                {
                    var row = idx / columncount;
                    var col = idx % columncount;
                    var targetheight = rowheights[row];

                    var b = new Rect(col * (targetwidth + padding) + outerpadding, row * (targetheight + padding), targetwidth, targetheight);
                    

                    if (visible != null && child is VisualElement e)
                    {
                        var vb = new Rect(b.X + X, b.Y + globaly, b.Width, b.Height);
                        var isvis = visible.Value.IntersectsWith(vb);
                        if (e.IsVisible != isvis)
                        {
                            
                            
                            e.IsVisible = isvis;
                        }
                    }
                    if(changed)
                        child.Arrange(b);
                    idx++;
                }
                return new Size(targetwidth * columncount + (padding * (columncount + 1)) + (outerpadding * 2), rowheights.Sum() + (padding * (rowcount + 1)));
            }
            finally
            {
                lastArrange = bounds;
                InArrange = false;
            }
        }

        public Size Measure(double widthConstraint, double heightConstraint)
        {
            var padding = Spacing;
            if (Children.Count == 0)
                return new Size(0, 0);

            var minwidth = Children.Max(c => c.MinimumWidth);
            var maxwidth = Children.Min(c => c.MaximumWidth);
            //var minheight = Children.Max(c => c.MinimumHeight);
            //var maxheight = Children.Min(c => c.MaximumHeight);

            var targetwidth = (minwidth + maxwidth) / 2;
            //var targetheight = (minheight + maxheight) / 2;

            if (double.IsNaN(targetwidth))
                targetwidth = 300;
            //if (double.IsNaN(targetheight))
            //    targetheight = 300;

            var columncount = (int)Math.Floor(widthConstraint / (targetwidth + padding));
            if(columncount == 0)
                columncount = 1;

            if (columncount == 1)
            {
                targetwidth = ((widthConstraint - padding) / 3) - padding;
                columncount = 3;
            }

            var rowcount = (int)Math.Ceiling(Children.Count / (double)columncount);
            var rowheights = new double[rowcount];
            for (var i = 0; i < rowcount; i++)
            {
                rowheights[i] = Children.Skip(i * columncount).Take(columncount).Max(c => c.Measure(targetwidth, double.MaxValue).Height);
            }


            return new Size(targetwidth * columncount + (padding * (columncount + 1)), rowheights.Sum() + (padding * (rowcount + 1)));
            //return new Size(columncount * targetwidth, rowcount * targetheight);
        }

        protected override ILayoutManager CreateLayoutManager()
        {
            return this;
        }
    }
}
