﻿using AudioBooks.Shared;
using AudioBooks.ViewModel;
using Maui.BindableProperty.Generator.Core;
namespace AudioBooks.Pages;

//[XamlCompilation(XamlCompilationOptions.Compile)]
public partial class Dashboard : ContentPage
{
    private DashboardModel model;
    
	ServerData Data;

    public Dashboard(ServerData data, DashboardModel vm)
	{
		Data = data;
		InitializeComponent();
		model = vm;
		BindingContext = model;
		
		
	}


	protected override void OnAppearing()
	{
		base.OnAppearing();
		model.IsLoading = true;
		model.ApplyFilterCommand.Execute(null);

    }


	private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
	{
		var view = (Components.BookCard)sender;
		var model = (BookCardModel)view.BindingContext;
		Shell.Current.GoToAsync(nameof(BookDetail), new Dictionary<string, object>
		{
			["Book"] =model.Book
		});
	}

	protected override void OnDisappearing()
	{
		base.OnDisappearing();
		//model.Books.Clear();
	}
}

