﻿global using System;

global using Maui.BindableProperty.Generator.Core;
global using CommunityToolkit.Mvvm.ComponentModel;

global using AudioBooks.Shared;
global using AudioBooks.ViewModel;