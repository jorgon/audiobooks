﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AudioBooks.Shared;
namespace AudioBooks
{
    public class Settings : ISettings
    {
        public string Host
        {
            get
            {
                return Preferences.Get("Host", "");
            }
            set
            {
                Preferences.Set("Host", value);
            }
        }

        public string Token
        {
            get
            {
                return Preferences.Get("AuthToken", "");
            }
            set
            {
                Preferences.Set("AuthToken", value);
            }
        }

        public string FileDestination
        {
            get
            {
                return Preferences.Get("FileDestination", "");
            }
            set
            {
                Preferences.Set("FileDestination", value);
            }
        }

        public bool HasHost => Preferences.ContainsKey("Host");

        public bool HasToken => Preferences.ContainsKey("AuthToken");

        public bool HasFileDestination => Preferences.ContainsKey("FileDestination");

        public void Reset()
        {
            Preferences.Clear();
        }
    }
}
