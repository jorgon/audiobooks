﻿using System.Collections.Concurrent;
using System.Text;

namespace AudioBooks
{
    public class ImageCacheService
    {
        static ImageCacheService()
        {
            var start = new ThreadStart(ProcessQueue);
     
            thread = new Thread(ProcessQueue);
            thread.IsBackground = true;
            thread.Start();
        }

        

        static string appdatadirectory = FileSystem.AppDataDirectory;

        //ImageSource Placeholder = ImageSource.FromResource("AudioBooks.Resources.bookplaceholder.png");
        public void Load(string url, Action<ImageSource> onupdate)
        {
            if(!url.StartsWith("http"))
            {
                onupdate(ImageSource.FromUri(new Uri(url)));
                return;
            }
            var ext = Path.GetExtension(url);
            var filename = Convert.ToBase64String(Encoding.ASCII.GetBytes(url));
            var path = Path.Combine(appdatadirectory, filename) + ext;

            
                queue.Enqueue((url, path, onupdate));

        }

        static Thread thread;
        static ConcurrentQueue<(string Source, string Dest, Action<ImageSource> OnUpdate)> queue = new();

        static Dictionary<string, FileImageSource> _cache = new Dictionary<string, FileImageSource>();

        static void ProcessQueue()
        {
            var client = new System.Net.WebClient();

            while(true)
            {
                if(queue.TryDequeue(out var item))
                {
                    FileImageSource img = null;
                    if (_cache.ContainsKey(item.Dest))
                    {
                        img = _cache[item.Dest];
                    }
                    else
                    {
                        if (!File.Exists(item.Dest))
                        {
                            client.DownloadFile(item.Source, item.Dest);
                        }
                        Thread.Sleep(50);
                        img = (FileImageSource)ImageSource.FromFile(item.Dest);
                        
                        _cache[item.Dest] = img;
                    }
                    item.OnUpdate(img);
                }
                else
                {
                    Thread.Sleep(250);
                }
            }
        }


    }
}
