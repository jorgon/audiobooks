﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AudioBooks
{
    partial class PlatformSettings
    {
        public string DocumentDirectory { get; internal set; }

        public PlatformSettings()
        {
            Init();

            if (!Directory.Exists(DocumentDirectory))
            {
                Directory.CreateDirectory(DocumentDirectory);
            }
        }
    }
}
