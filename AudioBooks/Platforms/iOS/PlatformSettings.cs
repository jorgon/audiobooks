﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace AudioBooks
{
    public partial class PlatformSettings
    {
        void Init()
        {
            DocumentDirectory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "AudioBooks");
        }

        public void Download(string url, string dest)
        {
            var client = new WebClient();

            ThreadPool.QueueUserWorkItem((o) => client.DownloadFile(url, dest));
        }
    }
}
