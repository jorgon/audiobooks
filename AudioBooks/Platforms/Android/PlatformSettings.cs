﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AudioBooks
{
    public partial class PlatformSettings
    {
        void Init()
        {
            
            //DocumentDirectory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonDocuments), "AudioBooks");
            DocumentDirectory = Path.Combine(Android.OS.Environment.ExternalStorageDirectory.Path, "AudioBooks");
            

        }

        public void Download(string url, string dest)
        {
            Android.App.DownloadManager.Request r = new Android.App.DownloadManager.Request(Android.Net.Uri.Parse(url));
            //r.SetDestinationUri(Android.Net.Uri.Parse("file://" + dest));
            r.SetDestinationInExternalPublicDir(Android.OS.Environment.DirectoryAudiobooks, dest.TrimStart('/'));
            r.SetDescription(dest);
            r.SetShowRunningNotification(true);
            r.SetNotificationVisibility(Android.App.DownloadVisibility.VisibleNotifyCompleted | Android.App.DownloadVisibility.Visible);
            var dm = (Android.App.DownloadManager)Android.App.Application.Context.GetSystemService(Android.Content.Context.DownloadService);

            dm.Enqueue(r);
        }
    }
}
