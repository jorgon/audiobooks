﻿using AudioBooks.ViewModel;

namespace AudioBooks
{
    public partial class AppShell : Shell
    {


        ISettings _settings;
        ServerData _serverdata;
        public AppShell(ISettings settings, ServerData serverdata, AppShellModel vm)
        {
            InitializeComponent();

            Routing.RegisterRoute(nameof(Pages.BookDetail), typeof(Pages.BookDetail));
            Routing.RegisterRoute(nameof(Pages.Authenticate), typeof(Pages.Authenticate));
            _settings = settings;
            _serverdata = serverdata;

            BindingContext = vm;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            
        }

       
    }
}