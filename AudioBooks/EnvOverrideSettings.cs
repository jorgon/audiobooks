﻿using AudioBooks.Shared;

namespace AudioBooks
{
    public class EnvOverrideSettings : ISettings
    {
        EnvSettings env = new EnvSettings();
        ISettings fallback;

        public EnvOverrideSettings(ISettings fallback)
        {
            this.fallback = fallback;
        }

        public string Host
        {
            get
            {
                return env.HasHost ? env.Host : fallback.Host;
            }
            set
            {
                if (env.HasHost)
                {
                    env.Host = value;
                }
                else
                {
                    fallback.Host = value;
                }
            }
        }
        public string Token
        {
            get
            {
                return env.HasToken ? env.Token : fallback.Token;
            }
            set
            {
                if (env.HasToken)
                {
                    env.Token = value;
                }
                else
                {
                    fallback.Token = value;
                }
            }
        }
        public string FileDestination
        {
            get
            {
                return env.HasFileDestination ? env.FileDestination : fallback.FileDestination;
            }
            set
            {
                if (env.HasFileDestination)
                {
                    env.FileDestination = value;
                }
                else
                {
                    fallback.FileDestination = value;
                }
            }
        }

        public bool HasHost => env.HasHost || fallback.HasHost;

        public bool HasToken => env.HasToken || fallback.HasToken;
        public bool HasFileDestination => env.HasFileDestination || fallback.HasFileDestination;

        public void Reset()
        {
            env.Reset();
            fallback.Reset();
        }
    }
}
