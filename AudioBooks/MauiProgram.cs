﻿using AudioBooks.Shared;
using Microsoft.Maui.Graphics.Platform;
using Microsoft.Maui.Handlers;
using Telerik.Maui.Controls.Compatibility;

#if IOS || MACCATALYST
using PlatformImage = UIKit.UIImage;
#elif MONOANDROID || ANDROID

using PlatformImage = Android.Graphics.Drawables.Drawable;
#elif WINDOWS
using PlatformImage = Microsoft.UI.Xaml.Media.ImageSource;
#elif TIZEN
using PlatformImage = Tizen.UIExtensions.ElmSharp.Image;
#elif (NETSTANDARD || !PLATFORM) || (NET6_0_OR_GREATER && !IOS && !ANDROID && !TIZEN)
using PlatformImage = System.Object;
#endif


namespace AudioBooks
{
    public static class MauiProgram
    {
        public static MauiApp CreateMauiApp()
        {
            var builder = MauiApp.CreateBuilder();
            builder
                .UseTelerik()
                
                .ConfigureFonts(fonts =>
                {
                    fonts.AddFont("OpenSans-Regular.ttf", "OpenSansRegular");
                    fonts.AddFont("OpenSans-Semibold.ttf", "OpenSansSemibold");
                    fonts.AddFont("fa-regular-400.ttf", "FA");
                    fonts.AddFont("fa-solid-900.ttf", "FA-S");
                    fonts.AddFont("materialdesignicons-webfont.ttf", "MA");
                    
                });

            
            builder.Services.AddSingleton<ISettings>(new EnvOverrideSettings(new Settings()));

            builder.Services.AddSingleton<ImageCacheService>();
            builder.Services.AddTransient<BookCardModel>();
            builder.Services.AddTransient<BookDetailModel>();
            builder.Services.AddTransient<DashboardModel>();
            builder.Services.AddTransient<SeriesModel>();
            builder.Services.AddTransient<AppShellModel>();
            

            builder.Services.AddTransient<Pages.BookDetail>();


            builder.Services.AddTransient<Pages.Dashboard>();
            builder.Services.AddTransient<Pages.Settings>();
            builder.Services.AddTransient<Pages.Queue>();
            builder.Services.AddTransient<Pages.Authenticate>();
            
            builder.Services.AddTransient<Pages.Components.BookCard>();

            builder.Services.AddSingleton<PlatformSettings>();

            DataJson.Directory = FileSystem.AppDataDirectory;

            builder.Services.AddSingleton<ServerData>((p) => {
                var settings = p.GetRequiredService<ISettings>();
                return new ServerData(settings.Host, settings.Token, t => settings.Token = t, new DataJson());
                });


#if ANDROID
            /*Dictionary<string, PlatformImage> imagecache = new();
            Dictionary<string, Android.Graphics.Bitmap> bmpcache = new();
            ImageHandler.Mapper.AppendToMapping(nameof(ImageView.Drawable),
                async (handler, view) =>
                {
                    string name = null;
                    if (view.Source is FileImageSource fis)
                    {
                        name = fis.File; 
                    }

                    PlatformImage image = null;
                    if (name == null || !imagecache.ContainsKey(name))
                    {
                        image = (await view.Source.GetPlatformImageAsync(handler.MauiContext)).Value;
                        Android.Graphics.Drawables.BitmapDrawable draw = image as Android.Graphics.Drawables.BitmapDrawable;

                        draw.Bitmap = Bitmap.CreateScaledBitmap(draw.Bitmap, 150, 150, false);
                        if(name != null)
                        {
                            imagecache[name] =  image;
                            bmpcache[name] = draw.Bitmap;
                        }
                    }
                    else if(imagecache.ContainsKey(name))
                    {
                        image = imagecache[name];
                    }
                    if (image != null)
                    {
                        handler.PlatformView.SetImageDrawable(image);
                        
                    }
                }
                );*/
                #endif
            return builder.UseMauiApp<App>().Build();

        }
    }
}