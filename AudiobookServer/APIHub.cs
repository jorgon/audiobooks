﻿using AudioBooks.Shared;
using AudioBooks.Shared.Messages;

namespace AudiobookServer
{
    public class APIHub : Microsoft.AspNetCore.SignalR.Hub
    {
        IData data;
        BackBlazeStorage Storage;

        static bool IsSetup = false;

        public APIHub(IData data, BackBlazeStorage storage)
        {
            Storage = storage;
            this.data = data;

            if(!IsSetup)
            {
                //DoSetup().GetAwaiter().GetResult();
                IsSetup = true;
            }
        } 

        async Task DoSetup()
        {
            var files = await Storage.GetFiles();
            var books = data.Books.ToArray(); ;
            var booklookup = books.ToDictionary(b => b.ID);
            var missingBooks = new HashSet<string>(books.Select(b => b.ID));

            
            foreach (var file in files)
            {
                var id = file.Split('.')[0];
                

                if(booklookup.ContainsKey(id))
                {
                    var book = booklookup[id];
                    book.ServerStatus = ServerStatus.Downloaded;
                    missingBooks.Remove(id);
                }
            }

            foreach(var missing in missingBooks)
            {
                if (booklookup.ContainsKey(missing))
                {
                    var book = booklookup[missing];
                    book.ServerStatus = ServerStatus.Unknown;
                }
            }
            await data.Update(books);
        }

        public override async Task OnConnectedAsync()
        {
            await base.OnConnectedAsync();
        }

        public async Task<string> AddUser(Authenticate authenticate)
        {
            var user = new User() { Username = authenticate.Username, Password = authenticate.Password };

            var token = user.AddToken();

            await data.Update(user);
            return token;
        }

        public async Task<AuthResult> Auth(Authenticate authenticate)
        {
            Console.WriteLine($"Auth {authenticate.Username} : {authenticate.Password}");
            var user = data.Users.FirstOrDefault(u => u.Username == authenticate.Username && u.Password == authenticate.Password);
            if (user == null)
            {
                Console.WriteLine($"Auth Not Found");
                return new AuthResult { Error = "User not found"};
            }
            var token = user.AddToken();
            Console.WriteLine($"New Token : {token}");
            await data.Update(user);
            return new AuthResult { Token = token };
        }

        async Task<User> _Auth(Message message)
        {
            if (string.IsNullOrWhiteSpace(message.Token))
                throw new FileNotFoundException();
            var token = message.Token;
            var user = data.Users.FirstOrDefault(u => u.Tokens.Contains(token));
            if (user == null)
                throw new FileNotFoundException();
            return user;
        }

        public async Task<List<Book>> GetBooks(GetBooks getbooks)
        {
            var user = await _Auth(getbooks);
            return data.Books.ToList();
        }

        public async Task<List<Series>> GetSeries(GetSeries getseries)
        {
            var user = await _Auth(getseries);
            return data.Series.ToList();
        }

        public async Task<List<Author>> GetAuthors(GetAuthors getauthors)
        {
            var user = await _Auth(getauthors);
            return data.Authors.ToList();
        }

        public async Task<User> GetProfile(GetBooks getbooks)
        {
            var user = await _Auth(getbooks);
            return user;
        }

        public async Task<Book[]> UpdateBook(UpdateBook updatebook)
        {
            await _Auth(updatebook);

            foreach(var book in updatebook.Books)
            {
                if(!book.ServerStatus.HasFlag(ServerStatus.Downloaded))
                {
                    if(File.Exists(book.GetInternalFileName("m4b")))
                        book.ServerStatus |= ServerStatus.Downloaded;
                }
            }

            return await data.Update(updatebook.Books);
        }

        public async Task UpdateSeries(UpdateSeries updateseries)
        {
            await _Auth(updateseries);
            await data.Update(updateseries.Series);
        }
        public async Task<Author[]> UpdateAuthors(UpdateAuthors updateauthors)
        {
            await _Auth(updateauthors);
            return await data.Update(updateauthors.Authors);
        }


        public static Dictionary<string, Book> UploadingBooks = new Dictionary<string, Book>();
        public async Task<string> StartUploadBookContent(string bookid)
        {
            var book = data.GetBook(bookid);
            string filename = book.GetInternalFileName("m4b");
            var guid = Guid.NewGuid().ToString();

            lock(UploadingBooks)
                UploadingBooks[guid] = book;

            return guid;
        }

        public async Task<string> GetDownloadUrl(GetDownloadUrl req)
        {
            await _Auth(req);
            var book = data.GetBook(req.BookID);
            return await Storage.GetDownloadUrl(book.GetInternalFileName("m4b"));
        }

        public async Task<UploadToken> GetUploadToken(GetUploadToken request)
        {
            await _Auth(request);
            return await Storage.GetUploadToken(request.Filename);
        }

        public async Task FinishUpload(FinishUpload finish)
        {
            await _Auth(finish);

            await Storage.FinishUpload(finish.FileId, finish.Sha1);
        }
    }
}
