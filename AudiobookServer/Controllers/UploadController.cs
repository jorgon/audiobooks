﻿using AudioBooks.Shared;
using Microsoft.AspNetCore.Mvc;

namespace AudiobookServer.Controllers
{
    public class UploadController : Controller
    {
        IStorage Storage;
        IData Data;

        public UploadController(IStorage storage, IData data)
        {
            Storage = storage;
            Data = data;
        }   

        [HttpPost]
        public async Task<IActionResult> Upload(string id)
        {

            Book book = null;

            lock (APIHub.UploadingBooks)
            {
                book = APIHub.UploadingBooks[id];
                APIHub.UploadingBooks.Remove(id);
            }

            await Storage.Upload(book.GetInternalFileName(".m4b"), Request.Body);
            book.ServerStatus |= ServerStatus.Downloaded;
            await Data.Update(book);
            return Ok();
        }
    }
}
