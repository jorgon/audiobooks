using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using AudioBooks.Shared;
using AudioBooks.Shared.Messages;

namespace AudiobookServer.Controllers
{/*
    [ApiController]
    [Route("[controller]")]
    public class DataController : ControllerBase
    {

        IData Data;
        private readonly ILogger<DataController> _logger;

        public DataController(IData data, ILogger<DataController> logger)
        {
            Data = data;
            _logger = logger;
        }

        [HttpPost]
        [Route("[action]")]
        public string Auth(Authenticate authenticate)
        {
            //var authenticate = await Load<Authenticate>();
            var user = Data.Users.FirstOrDefault(u => u.Username == authenticate.Username && u.Password == authenticate.Password);
            if(user == null)
                return null;
            var token = user.AddToken();
            Data.Get().Save();
            return token;
        }
        [HttpPost]
        [Route("[action]")]
        //public async Task<ActionResult<string>> AddUser()
        //public IActionResult AddUser([FromBody] Authenticate authenticate)
        public string AddUser(Authenticate authenticate)
        {
            //var authenticate = await Load<Authenticate>();
            var user = Data.Users.FirstOrDefault(u => u.Username == authenticate.Username);
            if(user != null)
                return null;

            user = new User()
            {
                Username = authenticate.Username,
                Password = authenticate.Password,
            };

            var token = user.AddToken();
            Data.Save(user);
            return token;
        }


        public User Auth(Message message)
        {
            if(string.IsNullOrWhiteSpace(message.Token))
                throw new FileNotFoundException();
            var token = message.Token;
            if (!Data.UsersByToken.TryGetValue(token, out var user))
                throw new FileNotFoundException();
            return user;
        }

        [HttpPost]
        [Route("[action]")]
        public Dictionary<string, Book> GetBooks(GetBooks getbooks)
        {
            Auth(getbooks);

            return Data.Get().Books;
        }

        [HttpPost]
        [Route("[action]")]
        public void Upload(Upload upload)
        {
            Auth(upload);
            long size;
            var book = Data.Get().Books[upload.Book];

            var filename = book.GetInternalFileName("tmp");
            using(var file = System.IO.File.Create(filename))
            {
                Request.Body.CopyTo(file);
                size = file.Position;
                
            }

            if(size == upload.Size)
            {
                System.IO.File.Move(filename, book.GetInternalFileName("m4b"));
                book.IsReady = true;
                Data.Get().Save();
            }
            
        }

        [HttpPost]
        [Route("[action]")]
        public Dictionary<string, Series> GetSeries(GetSeries getseries)
        {
            
            Auth(getseries);

            return Data.Get().Series;
        }

        [HttpPost]
        [Route("[action]")]
        public void UpdateBook(UpdateBook updatebook)
        {
            Auth(updatebook);

            var data = Data.Get();

            data.UpdateBook(updatebook.Books);
        }     
        
        [HttpPost]
        [Route("[action]")]
        public void UpdateSeries(UpdateSeries updateSeries)
        {
            Auth(updateSeries);

            var data = Data.Get();

            data.UpdateSeries(updateSeries.Series);
        }



        [HttpPost]
        [Route("[action]")]
        public void UpdateUser(UpdateUser updateuser)
        {
            var user = Auth(updateuser);

            var data = Data.Get();

            data.UpdateUser(user, updateuser.User);
        }

        [HttpPost]
        [Route("[action]")]
        public long GetUserBookProgress(GetUserBookProgress bookprogress)
        {
            var user = Auth(bookprogress);

            var data = Data.Get();

            return data.GetUserBookProgress(user, bookprogress.BookID);
        }

        [HttpPost]
        [Route("[action]")]
        public void UpdateUserBookProgress(UpdateUserBookProgress progress)
        {
            var user = Auth(progress);

            var data = Data.Get();

            data.UpdateUserBookProgress(user, progress.BookID, progress.Progress);
        }        
        
        [HttpPost]
        [Route("[action]")]
        public void UpdateUserBookFinished(UpdateUserBookFinished finished)
        {
            var user = Auth(finished);

            var data = Data.Get();

            data.UpdateUserBookFinished(user, finished.BookID, finished.Finished);
        }

        
        
        
    }*/
}