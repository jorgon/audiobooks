﻿using AudioBooks.Shared;
using AudioBooks.Shared.Messages;

namespace AudiobookServer
{
    public class BackBlazeStorage : IStorage
    {

        BackblazeStorageSettings _settings;
        string downloadUrl = null;
        ServerData ServerData = null;
        string accountID = null;

        Bytewizer.Backblaze.Client.BackblazeClient client = new();
        public BackBlazeStorage(BackblazeStorageSettings settings, ServerData server = null)
        {
            ServerData = server;
            _settings = settings;


            if (server == null)
            {
                var auth = client.Connect(_settings.KeyId, _settings.AppKey);
                downloadUrl = auth.DownloadUrl.ToString().TrimEnd('/');
                accountID = auth.AccountId;
            }
        }

        public async Task<string> GetDownloadUrl(string filename)
        {
            Console.WriteLine($"GetDownloadUrl {filename}");
            try
            {
                var buckets = await client.Buckets.GetAsync(new Bytewizer.Backblaze.Models.ListBucketsRequest(accountID) { BucketId=_settings.Bucket });
                var bucketname = buckets.First(b => b.BucketId == _settings.Bucket).BucketName;
                /*var auth = await client.Files.GetDownloadTokenAsync(_settings.Bucket, filename);
                auth.Response.*/
                return $"{downloadUrl}/file/{bucketname}/{filename.TrimStart('/')}";
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
                throw;
            }
        }

        public async Task<List<string>> GetFiles()
        {
            var ret = await client.Files.ListNamesAsync(_settings.Bucket);

            return ret.Response.Files.Select(f => f.FileName).ToList();
        }

        public async Task UploadViaServer(string filename, Stream stream)
        {
            var token = await ServerData.GetUploadToken(filename);

            var url = token.Url;
            var auth = token.Auth;
            var fileid = token.FileId;

            await DoUpload(fileid, url, auth, stream);    
        }

        async Task DoUpload(string fileid, Uri url, string auth, Stream stream)
        {
            var buffer = new byte[5000000];
            var progress = new Progress();
            List<int> parts = new List<int>();
            List<string> sha1 = new List<string>();

            int i = 1;

            while (stream.Position < stream.Length)
            {
                int length = stream.Read(buffer, 0, buffer.Length);

                // var hash = Convert.ToHexString(SHA1.HashData(buffer));
                // sha1.Add(hash);

                if (!parts.Contains(i))
                {


                    MemoryStream part = new MemoryStream(buffer, 0, length, false, false);
                    int failcount = 0;

                    while (failcount < 10)
                    {
                        try
                        {
                            var result = await client.Parts.UploadAsync(url, i, auth, part, progress);
                            sha1.Add(result.Response.ContentSha1);

                            break;
                        }
                        catch (Exception ex)
                        {
                            failcount++;
                            Thread.Sleep(10000);
                        }
                    }

                }
                i++;
            }

            var uploadresponse = await client.Parts.FinishLargeFileAsync(fileid, sha1);
        }

        public async Task Upload(string filename, Stream stream)
        {
            if(ServerData == null)
            {
                await UploadNative(filename, stream);
            }
            else
            {
                await UploadViaServer(filename, stream);
            }
        }
        async Task UploadNative(string filename, Stream stream)
        {

            var buffer = new byte[5000000];

            if (buffer.Length >= stream.Length)
            {
                await client.UploadAsync(_settings.Bucket, filename, stream);
            }
            else
            {


                var meta = await client.Parts.StartLargeFileAsync(_settings.Bucket, filename);
                var fileid = meta.Response.FileId;


                var urlresponse = await client.Parts.GetUploadUrlAsync(fileid);
                var url = urlresponse.Response.UploadUrl;
                var auth = urlresponse.Response.AuthorizationToken;

                await DoUpload(fileid, url, auth, stream);
            }


        }

        internal async Task<UploadToken> GetUploadToken(string filename)
        {
            var response = await client.Parts.StartLargeFileAsync(_settings.Bucket, filename);
            var url = await client.Parts.GetUploadUrlAsync(response.Response.FileId);

            return new UploadToken
            {
                Url = url.Response.UploadUrl,
                Auth = url.Response.AuthorizationToken,
                FileId = response.Response.FileId

            };
        }

        internal async Task FinishUpload(string fileId, List<string> sha1)
        {
            await client.Parts.FinishLargeFileAsync(fileId, sha1);
        }

        class Progress : IProgress<Bytewizer.Backblaze.Models.ICopyProgress>
        {
            public void Report(Bytewizer.Backblaze.Models.ICopyProgress value)
            {

            }
        }

    }
}
