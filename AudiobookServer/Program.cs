using AudioBooks.Shared;
using AudiobookServer;
using Microsoft.Extensions.DependencyInjection;


if (Environment.OSVersion.Platform != PlatformID.Win32NT)
{
    Book.DestinationPath = "";
}


var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Host.UseSystemd();

builder.Services.AddSingleton<IRedisSettings>();
builder.Services.AddSingleton<BackblazeStorageSettings>();
builder.Services.AddSingleton<BackBlazeStorage>();
builder.Services.AddSingleton<IData, RedisData>();
builder.Services.AddSingleton<IStorage, BackBlazeStorage>();

builder.Logging.AddConsole();
builder.Services.AddControllers();
var signal = builder.Services.AddSignalR();
signal.AddHubOptions<APIHub>(config =>
{
    config.MaximumReceiveMessageSize = 1024 * 1024;
});
var app = builder.Build();

app.UseHttpLogging();
// Configure the HTTP request pipeline.

app.UseHttpsRedirection();

app.UseRouting().UseEndpoints(opts =>
{
    opts.MapDefaultControllerRoute();
});

app.MapHub<APIHub>("api", config =>
{
    config.ApplicationMaxBufferSize= 1024*1024;
    config.TransportMaxBufferSize= 1024*1024;
    
});

//app.MapControllers();

app.Run();
