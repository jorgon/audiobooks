﻿namespace AudiobookServer
{
    public class IRedisSettings
    {
#if DEBUG
        public string Host { get; set; } = "40.117.42.170:6379";
#else
  public string Host { get; set; } = "127.0.0.1:6379";
#endif
        public int Database { get; set; } = 1;
    }
}