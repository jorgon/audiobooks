﻿using AudioBooks.Shared;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using StackExchange.Redis;

namespace AudiobookServer
{
    public class RedisData : IData
    {
        ConnectionMultiplexer redis;
        IDatabase db;
        IServer server;
        private readonly IRedisSettings settings;

        public RedisData(IRedisSettings settings)
{
            var options = ConfigurationOptions.Parse(settings.Host);
            options.SyncTimeout = 10000;
            options.AsyncTimeout = 10000;
            redis = ConnectionMultiplexer.Connect(options);
            db = redis.GetDatabase(settings.Database);
            var eps = redis.GetEndPoints();
            server = redis.GetServer(eps[0].ToString());
            this.settings = settings;
        }

        string GetName<T>()
        {
            var name = typeof(T).Name;
            return name + "_";
        }

        string GetKey<T>(string id)
        {
            return GetName<T>() +id;
        }

        async Task Save<T>(string id, T value)
        {
            var key = GetKey<T>(id);
            var val = JsonConvert.SerializeObject(value);

            await db.StringSetAsync(key, val);
            await db.SetAddAsync(GetName<T>(), key);
        }

        IQueryable<T> GetQuery<T>()
        {
            var name = GetName<T>();
            List<T> ret = new List<T>();

            ;

            foreach (var val in db.StringGet(db.SetMembers(name).Select(k => (RedisKey)k.ToString()).ToArray()))
            {
                ret.Add(JsonConvert.DeserializeObject<T>(val));
            }
            return ret.AsQueryable();
        }

        public override IQueryable<User> Users => GetQuery<User>();

        public override IQueryable<Book> Books => GetQuery<Book>();

        public override IQueryable<Series> Series => GetQuery<Series>();

        public override IQueryable<Author> Authors => GetQuery<Author>();

        protected override Task Save(User user)
        {
            return Save(user.Username, user);
        }

        protected override async Task Save(IEnumerable<Series> series)
        {
            foreach(var s in series)
            {
                await Save(s.ID, s);
            }
        }

        protected override async Task Save(IEnumerable<Book> books)
        {
            foreach (var s in books)
            {
                await Save(s.ID, s);
            }
        }

        protected override async Task Save(IEnumerable<Author> authors)
        {
            foreach (var s in authors)
            {
                await Save(s.ID, s);
            }
        }
    }
}
