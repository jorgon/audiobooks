﻿using Bytewizer.Backblaze.Client;
using Bytewizer.Backblaze.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;


public static class Extensions
{
    public static void Replace<T>(this List<T> source, List<T> dest)
    {
        if(source != dest)
        {
            dest.Clear();
            dest.AddRange(source);
        }
    }

    public static void Update<T>(this List<T> source, List<T> dest)
    {
        if (source != dest)
        {
            foreach(var src in source)
            {
                if(!dest.Contains(src))
                    dest.Add(src);
            }
        }
    }

    public static string CleanHtml(this string text)
    {
        return Regex.Replace(
                text.Replace("\r", "").Replace("\n", "").Replace("<p>", "\n").Trim(' ', '\t', '\r', '\n'),
                "<(.*?)>", "");
    }

    public static async Task<List<FileItem>> ListAllFilesAsync(this IStorageFiles files, string bucketid)
    {
        string start = null;
        List<FileItem> ret = new List<FileItem>();
        IApiResults<ListFileNamesResponse> response = null;
        do
        {
            response = await files.ListNamesAsync(new ListFileNamesRequest(bucketid) { StartFileName = start });

            ret.AddRange(response.Response.Files);

            start = response.Response.NextFileName;


        } while (start != null);

        return ret;
    }
}