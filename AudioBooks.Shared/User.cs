﻿namespace AudioBooks.Shared
{
    public class User
    {
        public string Username;
        public string Password;
        public List<string> Finished { get; } = new List<string>();
        public List<string> Tokens = new List<string>();
        public List<string> Queue = new List<string>();

        public List<UserBook> Books = new List<UserBook>();

        public string AddToken()
        {
            var token = Guid.NewGuid().ToString();
            Tokens.Add(token);
            return token;

        }

        public UserBook GetBook(string id)
        {
            var book = Books.FirstOrDefault(b => b.ID == id);
            if (book != null)
                return book;
            book = new UserBook();
            book.ID = id;
            Books.Add(book);
            return book;
        }

        public void Update(User user)
        {
            Username = user.Username;
            Password = user.Password;

            user.Finished.Replace(Finished);
            user.Tokens.Replace(Tokens);
            user.Books.Replace(Books);
            
        }
    }
}
