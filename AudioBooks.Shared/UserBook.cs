﻿namespace AudioBooks.Shared
{
    public class UserBook
    {
        public string ID;
        public ClientStatus ClientStatus = ClientStatus.Unknown;
        public long Position = 0;
    }
}
