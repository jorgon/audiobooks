﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AudioBooks.Shared
{
    [Flags]
    public enum ServerStatus
    {
        Unknown = 0x0,
        Downloaded = 0x1,
    }
}
