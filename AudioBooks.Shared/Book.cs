﻿using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace AudioBooks.Shared
{
    public class Book
    {
        public static string DestinationPath = "\\\\sink\\Shared\\Audiobooks\\Server";
        public string ID { get; set; }
        public string Summary { get; set; }
        public List<string> AuthorIds { get; set; } = new List<string>();
        public string Title { get; set; }
        public string ImageUrl { get; set; }
        public string Cover => ID + ".jpg";
        public string Reader { get; set; }
        public bool IsPodcast { get; set; } = false;
        public List<string> SeriesIDs { get; set; } = new List<string>();
        public List<string> Tags { get; set; } = new List<string>();

        public ServerStatus ServerStatus { get; set; } = ServerStatus.Unknown;

        public bool IsAvailable => ServerStatus.HasFlag(ServerStatus.Downloaded);

        public float AvailableOpacity => IsAvailable ? 1.00f : .40f;

        public Book Update(Book book)
        {
            Summary = book.Summary;
            book.AuthorIds.Replace(AuthorIds);
            Title = book.Title;
            ImageUrl = book.ImageUrl;
            Reader = book.Reader;
            IsPodcast = book.IsPodcast;
            //ServerStatus |= book.ServerStatus;
            ServerStatus = book.ServerStatus;
            book.SeriesIDs.Replace(SeriesIDs);
            book.Tags.Update(Tags);
            return this;
        }


        public string GetOverrideFilename()
        {
            return Path.Combine("_Overrides", ID + ".txt");
        }

        static List<string> GetMatches(string src, string pattern)
        {
            var ret = new List<string>();
            foreach (Match match in Regex.Matches(src, pattern, RegexOptions.Singleline))
            {
                ret.Add(match.Value);
            }
            return ret;
        }
        static Dictionary<string, string> Match(string src, params string[] patterns)
        {
            var ret = new Dictionary<string, string>();

            foreach (var pattern in patterns)
            {
                var match = Regex.Match(src, pattern, RegexOptions.ExplicitCapture | RegexOptions.Singleline);
                foreach (Group group in match.Groups)
                {
                    ret[group.Name] = group.Value;
                }
            }

            return ret;
        }

        public static IEnumerable<Book> ParseSeriesBooks(string page, IData idata)
        {

            var idpattern = "product-list-flyout-(?<ID>.*?)\"";

            
            var imageurlpattern = "<picture(.*?)src=\"(?<img>.*?)\"";
            var titlepattern = "bc-color-base([ \\n\\r]*?)bc-text-bold\" >(?<title>.*?)<";
            var booksummarypattern = "bc-color-base\">(?<summary>.*?)</p>";
            //var booksummarypattern = "product-list-flyout-(.*?)<p>(?<summary>.*?)</p>";

            var authorpattern = "href=\"/author/(.*?)/(?<authorid>.*?)\\?ref(.*?)>(?<author>.*?)</a>";



            var narratorpattern = "searchNarrator(.*?)>(?<narrator>.*?)<";

            var summarypattern = "series-summary-content(.*?)>(?<summary>.*?)</div>";

            var matches = GetMatches(page, "productListItem(.*?)ratingsLabel(.*?)bc-divider-secondary");

            foreach (var match in matches)
            {
                if (match.Contains("Not available on audible.com"))
                    continue;
                var data = Match(match, idpattern, authorpattern, imageurlpattern, titlepattern, booksummarypattern, narratorpattern);

                var authors = new List<Author>();

                var authorsresult = Regex.Matches(match.ToString(), authorpattern, RegexOptions.Singleline | RegexOptions.ExplicitCapture | RegexOptions.Compiled);

                foreach (Match author in authorsresult)
                {
                    authors.Add( new Author() { ID = author.Groups[1].Value, Name = author.Groups[2].Value });
                }
                idata.Update(authors).GetAwaiter().GetResult();
                //Task.Run(() => );

                var ret =  new Book()
                {
                    ID = data["ID"]?.Trim(),
                    //Author = data["author"]?.Trim(),
                    ImageUrl = data["img"]?.Trim(),
                    Title = data["title"]?.Trim(),
                    Summary = data["summary"]?.CleanHtml(),
                    //SeriesID = series,
                    Reader = data["narrator"].Trim(),
                    ServerStatus = ServerStatus.Unknown,
                    //seriesbook = data["seriesbook"]?.Trim()
                };

                authors.Select(a => a.ID).ToList().Replace(ret.AuthorIds);

                yield return ret;
            }
        }

        public static IEnumerable<Book> ParseBooks(string page, IData idata)
        {
            var pattern = "<div id=\"adbl-library-content-row-(.*?)bc-divider-secondary\">";


            foreach (var segment in Regex.Matches(page, pattern, RegexOptions.Singleline))
            {
                var part = segment.ToString();

                var idpattern = "adbl-library-content-row-(?<asin>.*?)\" class";
                var imgpattern = "img(.*?)src=\"(?<img>.*?)\"";
                var titlepattern = "bc-size-headline3\"  >(?<title>.*?)</span>";
                var authorpattern = "href=\"/author/(?<authorid>.*?)\\?ref(.*?)>(?<author>.*?)</a>";
                var narratorpattern = "narratorLabel(.*?)bc-size-callout(.*?)>(?<narrator>.*?)<";
                var summarypattern = "merchandisingSummary(.*?)>(?<summary>.*?)</";

                var data = Match(part, idpattern, imgpattern, titlepattern, narratorpattern, summarypattern);

                var podcastpattern = "adbl-episodes-link(.*?)href=\"(?<podcasturl>.*?)\"";

                var seriesresult = Regex.Matches(segment.ToString(), "(/series/(.*?)/(?<seriesid>.*?)\\?)", RegexOptions.Singleline | RegexOptions.ExplicitCapture | RegexOptions.Compiled);
                var authorsresult = Regex.Matches(segment.ToString(), authorpattern, RegexOptions.Singleline | RegexOptions.ExplicitCapture | RegexOptions.Compiled);

                var authors = new List<Author>();

                foreach (Match author in authorsresult)
                {
                    authors.Add(new Author() { ID = author.Groups[1].Value, Name = author.Groups[2].Value });
                }



                List<string> series = new List<string>();
                foreach (Match sresult in seriesresult)
                {
                    series.Add(sresult.Groups[1].Value);
                }
                data.TryGetValue("narrator", out var narrator);
                //data["series"]?.Trim(),
                var ret = new Book()
                {
                    ID = data["asin"]?.Trim(),
                    ImageUrl = data["img"]?.Trim(),
                    Title = data["title"]?.Trim(),
                    Summary = data["summary"]?.Trim("<p >".ToCharArray()),
                    Reader = narrator?.Trim(),
                    ServerStatus = ServerStatus.Unknown,
                };

                series.Replace(ret.SeriesIDs);
                authors.Select(a => a.ID).ToList().Replace(ret.AuthorIds);

                idata.Update(authors).GetAwaiter().GetResult();
                //Task.Run(() => idata.Update(authors));
                yield return ret;


            }
        }

        public string GetInternalFileName(string ext)
        {
            List<string> parts = new List<string>();
            parts.Add(DestinationPath);
            if (!ext.StartsWith("."))
                ext = "." + ext;
            parts.Add(ID + ext);
            return string.Join(Path.DirectorySeparatorChar, parts);
        }
        public string GetFileName(string path, string ext=".m4b", bool limit=true)
        {
            List<string> parts = new List<string>();
            parts.Add(path);
            if (!ext.StartsWith("."))
                ext = "." + ext;
            if (!".m4b".Contains(ext))
                parts.Add(".meta");

            //parts.Add(author);

            string title = this.Title;

            if (title.Contains(':'))
            {
                var idx = title.IndexOf(':');
                title = this.Title.Substring(0, idx);
            }

            if (limit)
            {
                if (title.Length > 50)
                {
                    var idx = this.Title.LastIndexOf(' ', 50);
                    title = this.Title.Substring(0, idx).Trim();
                }
            }

            var filename = title + ext;
            

            foreach (var c in ":<>\"/|?*")
            {
                filename = filename.Replace(c, ',');
            }

            parts.Add(filename);

            var ret = string.Join(Path.DirectorySeparatorChar, parts);
            /*foreach (var c in "'!")
            {
                ret = ret.Replace(c.ToString(), "");
            }*/

            return ret;
        }

        public string GetAuthorNames(IData data)
        {
            return string.Join(", ", AuthorIds.Select(id => data.GetAuthor(id).Name));
        }

        public bool IsDownloaded(string path)
        {
            var filename = GetFileName(path);
            return File.Exists(filename);
        }
    }
}
