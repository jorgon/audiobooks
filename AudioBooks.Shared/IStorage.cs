﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AudioBooks.Shared
{
    public interface IStorage
    {
        //void Delete(string id);
        Task<List<string>> GetFiles();
        Task<string> GetDownloadUrl(string filename);
        Task Upload(string filename, Stream stream);
    }
}
