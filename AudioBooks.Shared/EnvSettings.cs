﻿

namespace AudioBooks.Shared
{
    public class EnvSettings : ISettings
    {
        Dictionary<string, string> values = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);
        public EnvSettings()
        {
            Reset();            
        }

        string Get(string name)
        {
            if (!values.ContainsKey(name))
                return null;
            return values[name];
        }

        public string Host { get => Get("Host"); set => values["Host"] = value; }
        public string Token { get => Get("Token"); set => values["Token"] = value; }
        public string FileDestination { get => Get("FileDestination"); set => values["FileDestination"] = value; }

        public bool HasHost => values.ContainsKey("Host");

        public bool HasToken => values.ContainsKey("Token");

        public bool HasFileDestination => values.ContainsKey("FileDestination");

        public void Reset()
        {
            var env = Environment.GetEnvironmentVariables();

            foreach (var key in env.Keys)
            {
                values[key.ToString()] = env[key].ToString();
            }
        }
    }
}
