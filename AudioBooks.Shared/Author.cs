﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AudioBooks.Shared
{
    public class Author
    {
        public string ID { get; set; }
        public string Name { get; set; }

        public void Update(Author author)
        {
            ID = author.ID;
            Name = author.Name;
        }
    }
}
