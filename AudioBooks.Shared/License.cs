﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AudioBooks.Shared
{
    public class License
    {
        public ContentLicense content_license;
        public List<string> response_groups;
    }

    public class ContentLicense
    {
        public string access_expiry_date;
        public string acr;
        public string asin;
        public ContentMetadata content_metadata;
        public string drm_type;
        public string license_id;
        public string license_response;
        public string message;
        public string request_id;
        public string status_code;
    }

    public class ContentMetadata
    {
        public ChapterInfo chapter_info;
        public ContentReference content_reference;
        public LastPositionHeard last_position_heard;
    }

    public class ChapterInfo
    {
        public long brandIntroDurationMs;
        public long brandOutroDurationMs;
        public List<Chapter> chapters;
        public bool is_accurate;
        public long runtime_length_ms;
        public long runtime_length_sec;
    }

    public class Chapter
    {
        public long length_ms;
        public long start_offset_ms;
        public long start_offset_sec;
        public string title;
    }

    public class ContentReference
    {
        public string acr;
        public string asin;
        public string content_format;
        public long content_size_in_bytes;
        public string file_version;
        public string marketplace;
        public string sku;
        public string tempo;
        public string version;
    }

    public class LastPositionHeard
    {
        public long position_ms;
        public string status;
    }
}
