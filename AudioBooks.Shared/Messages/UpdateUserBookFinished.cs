﻿namespace AudioBooks.Shared
{

    namespace Messages
    {
        public class UpdateUserBookFinished : Message
        {
            public string BookID { get; set; }
            public bool Finished { get; set; }

            public UpdateUserBookFinished(string bookid, bool finished)
            {
                BookID = bookid;
                Finished = finished;
            }
        }
    }
}
