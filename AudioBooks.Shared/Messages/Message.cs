﻿namespace AudioBooks.Shared
{

    namespace Messages
    {
        public abstract class Message
        {
            public string Token { get; set; }
        }
    }
}
