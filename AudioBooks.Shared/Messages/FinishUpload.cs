﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AudioBooks.Shared.Messages
{
    public class FinishUpload : Message
    {
        public FinishUpload(string fileId, List<string> sha1)
        {
            FileId = fileId;
            Sha1 = sha1;
        }

        public string FileId { get; }
        public List<string> Sha1 { get; }
    }
}
