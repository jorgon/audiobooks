﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AudioBooks.Shared.Messages
{
    public class UploadToken
    {

        public string Auth { get; set; }
        public string FileId { get; set; }
        public Uri Url { get; set; }
    }
}
