﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AudioBooks.Shared.Messages
{
    public  class UpdateAuthors : Message
    {
        public UpdateAuthors(Author[] authors)
        {
            Authors = authors;
        }

        public Author[] Authors { get; set; }
    }
}
