﻿namespace AudioBooks.Shared
{

    namespace Messages
    {
        public class UpdateUserBookProgress : Message
        {
            public string BookID { get; set; }
            public long Progress { get; set; }

            public UpdateUserBookProgress(string bookid, long progress)
            {
                BookID = bookid;
                Progress = progress;
            }
        }
    }
}
