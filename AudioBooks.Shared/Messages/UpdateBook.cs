﻿using Newtonsoft.Json;

namespace AudioBooks.Shared
{

    namespace Messages
    {
        public class UpdateBook : Message
        {
            public Book[] Books { get; set; }

            [JsonConstructor]
            public UpdateBook()
            {

            }

            public UpdateBook(Book book)
            {
                Books = new Book[] { book };
            }  
            
            public UpdateBook(Book[] books)
            {
                Books = books;
            }
        }
    }
}
