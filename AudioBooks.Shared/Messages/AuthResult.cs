﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AudioBooks.Shared.Messages
{
    public class AuthResult
    {
        public string Token { get; set; } = null;
        public string Error { get; set; } = null;

    }
}
