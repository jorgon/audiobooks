﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace AudioBooks.Shared.Messages
{
    public class GetUploadToken : Message
    {
        public string Filename { get; set; }

        public GetUploadToken()
        {

        }

        public GetUploadToken(string filenamne)
        {
            Filename = filenamne;
        }
    }
}
