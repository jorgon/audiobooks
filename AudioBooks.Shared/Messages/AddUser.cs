﻿namespace AudioBooks.Shared
{

    namespace Messages
    {
        public class AddUser : Message
        {
            public string Username { get; set; }
            public string Password { get; set; }
        }
    }
}
