﻿namespace AudioBooks.Shared
{

    namespace Messages
    {
        public class UpdateUser : Message
        {
            public User User { get; set; }

            public UpdateUser(User user)
            {
                User = user;
            }
        }
    }
}
