﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AudioBooks.Shared.Messages
{
    public class Upload : Message
    {
        public string Book { get; set; }
        public long Size { get; set; }

        [JsonConstructor]
        public Upload()
        {

        }

        public Upload(string book, long size)
        {
            Book = book;
            Size = size;
        }


    }
}
