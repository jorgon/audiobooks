﻿namespace AudioBooks.Shared
{

    namespace Messages
    {
        public class DownloadBook : Message
        {
            public string BookID { get; set; }
            public string Cookie { get; set; }

            public DownloadBook(string bookid, string cookie)
            {
                BookID = bookid;
                Cookie = cookie;
            }
        }
    }
}
