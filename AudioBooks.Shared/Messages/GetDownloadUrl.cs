﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AudioBooks.Shared.Messages
{
    public class GetDownloadUrl : Message
    {
        public string BookID { get; set; }

        public GetDownloadUrl(string bookID)        {
            BookID = bookID;
        }                      public GetDownloadUrl()        {
        }

    }
}
