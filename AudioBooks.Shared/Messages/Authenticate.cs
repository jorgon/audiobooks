﻿namespace AudioBooks.Shared
{

    namespace Messages
    {
        public class Authenticate : Message
        {
            public string Username { get; set; }
            public string Password { get; set; }


            public Authenticate(string username, string password, string token = null)
            {
                Username = username;
                Password = password;
                Token = token;
            }
        }
    }
}
