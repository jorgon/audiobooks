﻿namespace AudioBooks.Shared
{

    namespace Messages
    {
        public class GetUserBookProgress : Message
        {
            public string BookID { get; set; }

            public GetUserBookProgress(string bookid)
            {
                BookID = bookid;
            }
        }
    }
}
