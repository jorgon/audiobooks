﻿namespace AudioBooks.Shared
{

    namespace Messages
    {
        public class UpdateSeries : Message
        {
            public Series[] Series { get; set; }

            public UpdateSeries(Series[] series)
            {
                Series = series;
            }
        }
    }
}
