﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AudioBooks.Shared
{
    public class PlaylistItem
    {
        public string path;
        public long start;
        public long length;

        public void ParseRange(string line)
        {
            var range = line.Split(":@".ToCharArray());
            start = long.Parse(range[2]);
            length = long.Parse(range[1]);
        }

    }
}
