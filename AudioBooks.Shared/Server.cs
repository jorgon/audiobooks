﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AudioBooks.Shared.Messages;
using Microsoft.AspNetCore.SignalR.Client;
using Newtonsoft.Json;

namespace AudioBooks.Shared
{
    public class ServerData : IData
    {
        Microsoft.AspNetCore.SignalR.Client.HubConnection client;
       //System.Net.Http.HttpClient webclient;
        public string Host { get; set; }
        public User User { get; set; }
        string authtoken;
        Action<string> updateAuthToken;
        IData Cache;

        public override IQueryable<User> Users => Cache.Users;

        public override IQueryable<Book> Books => Cache.Books;

        public override IQueryable<Series> Series => Cache.Series;
        public override IQueryable<Author> Authors => Cache.Authors;

        public ServerData(string host, string authtoken, Action<string> updateAuthToken, IData cache)
        {
            Cache = cache;
            client = new HubConnectionBuilder()
                .WithUrl($"{host}/api")
                .WithAutomaticReconnect()
                .Build();
            
            //webclient = new System.Net.Http.HttpClient();
            this.Host = host;
            this.authtoken = authtoken;
            this.updateAuthToken = updateAuthToken;

            client.On<Book, string>("BookEvent", BookEvent);
            
        }

        void BookEvent(Book book, string message)
        {

        }

        

        public async Task Sync()
        {
            //try
            {
                await Cache.Update(await GetBooks());
                await Cache.Update(await GetSeries());
                await Cache.Update(await GetAuthors());
            }
            /*catch(Exception ex)
            {
                ex.ToString();
            }*/
        }

        async Task Reconnect()
        {
            try
            {
                if (client.State == HubConnectionState.Disconnected)
                {
                    client = new HubConnectionBuilder()
                        .WithUrl($"{Host}/api")
                        .WithAutomaticReconnect()
                        .Build();
                    client.On<Book, string>("BookEvent", BookEvent);
                    await client.StartAsync();
                    
                }
            }
            catch(Exception e)
            {
                e.ToString();
            }
        }

        async Task<RetType> Post<RetType>(string action, Message data=null)
        {
            try
            {
                
                await Reconnect();
                if (data == null)
                    return await client.InvokeAsync<RetType>(action);
                data.Token = authtoken;
                return await client.InvokeAsync<RetType>(action, data);
            }
            catch(Exception ex)
            {
                ex.ToString();
                throw;
            }
        }

        async Task Post(string action, Message data = null)
        {
            await Reconnect();
            if (data == null)
            {
                await client.InvokeAsync(action);
            }
            else
            {
                data.Token = authtoken;
                await client.InvokeAsync(action, data);
            }
        }

        public async Task<string> GetDownloadUrl(Book book)
        {
            return await Post<string>("GetDownloadUrl", new GetDownloadUrl(book.ID));
        }

        public async Task UploadBookContent(Book book, string destfilename)
        {
            await Reconnect();
            
            var id = await Post<UploadToken>("GetUploadToken", new GetUploadToken(book.ID + ".m4b"));
            var bb = new Bytewizer.Backblaze.Client.BackblazeClient();

            using (var stream = File.OpenRead(destfilename))
            {
                Console.WriteLine($"Uploading {book.Title} ({new FileInfo(destfilename).Length})");
                var buffer = new byte[5000000];

                if (buffer.Length >= stream.Length)
                {
                    //await client.UploadAsync(settings.Bucket, filename, stream);
                    throw new NotImplementedException();
                }
                else
                {
                    List<int> parts = new List<int>();
                    List<string> sha1 = new List<string>();



                    var url = id.Url;
                    var token = id.Auth;

                    var progress = new Progress();


                    int i = 1;

                    while (stream.Position < stream.Length)
                    {
                        int length = stream.Read(buffer, 0, buffer.Length);

                        // var hash = Convert.ToHexString(SHA1.HashData(buffer));
                        // sha1.Add(hash);

                        if (!parts.Contains(i))
                        {
                            Console.WriteLine($"{book.Title} ( {Math.Round(stream.Position / 1048576f, 2)} / {Math.Round(stream.Length / 1048576f, 2)}) {Math.Round(Math.Round(stream.Position / (double)stream.Length, 2) * 100)}%");

                            MemoryStream part = new MemoryStream(buffer, 0, length, false, false);

                            int failcount = 0;

                            while (failcount < 10)
                            {
                                try
                                {
                                    var result = await bb.Parts.UploadAsync(url, i, token, part, progress);
                                    sha1.Add(result.Response.ContentSha1);

                                    break;
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine(ex.Message);
                                    failcount++;
                                    System.Threading.Thread.Sleep(10000);
                                }
                            }

                        }
                        i++;
                    }
                    await Post("FinishUpload", new FinishUpload(id.FileId, sha1));
                    
                }

            }
            //await server.UploadBookContent(book, path);
            book.ServerStatus |= ServerStatus.Downloaded;
            await Update(book);



        
        }

        public async Task<AuthResult> Auth(string username, string password)
        {
            var token = await Post<AuthResult>("Auth", new Authenticate(username, password ));
            if (!string.IsNullOrEmpty(token.Token))
            {
                this.authtoken = token.Token;
                updateAuthToken(token.Token);
            }
            return token;
        }

        public async Task<string> AddUser(string username, string password)
        {
            var token = await Post<string>("AddUser", new Authenticate(username, password));
            this.authtoken = token;
            updateAuthToken(token);
            return token;
        }

        public async Task<List<Book>> GetBooks()
        {
            return await Post<List<Book>>("GetBooks", new GetBooks());
            
        }
        
        public async Task<List<Author>> GetAuthors()
        {
            return await Post<List<Author>>("GetAuthors", new GetAuthors());
            
        }

        public async Task<User> GetProfile()
        {
            return await Post<User>("GetProfile", new GetUser());
        }

        public async Task<List<Series>> GetSeries()
        {
            return await Post<List<Series>>("GetSeries", new GetSeries());
        }

        
        public async Task<Book> UpdateBook(Book book)
        {
            return (await Post<Book[]>("UpdateBook", new UpdateBook(book)))[0];
        }        
        
        public async Task<Book[]> UpdateBooks(Book[] books)
        {
            return await Post<Book[]>("UpdateBook", new UpdateBook(books ));
        } 
        
        public async Task<Author[]> UpdateAuthors(Author[] authors)
        {
            return await Post<Author[]>("UpdateAuthors", new UpdateAuthors(authors));
        }

        
        public async Task UpdateSeries(Series[] series)
        {
            await Post<string>("UpdateSeries", new UpdateSeries(series));
        }


        public  async Task UpdateUser(User user)
        {
            await Post<string>("UpdateUser", new UpdateUser(user));
        }

        public async Task<long> GetUserBookProgress(string bookid)
        {
            return await Post<long>("UpdateUser", new GetUserBookProgress(bookid));
        }

        public async Task UpdateUserBookProgress(string bookid, long progress)
        {
            await Post<string>("UpdateUserBookProgress", new UpdateUserBookProgress(bookid, progress));
        }

        public async Task UpdateUserBookFinished(string bookid, bool finished)
        {
            await Post<string>("UpdateUserBookFinished", new UpdateUserBookFinished(bookid, finished));
        }

        public async Task DownloadBook(string bookid, string cookie)
        {
            await Post<string>("DownloadBook", new DownloadBook(bookid, cookie));
        }

        protected override async Task Save(User user)
        {
            await UpdateUser(user);
            await Cache.Update(user);

        }

        protected override async Task Save(IEnumerable<Series> series)
        {
            await UpdateSeries(series.ToArray());
            await Cache.Update(series);
        }

        protected override async Task Save(IEnumerable<Book> books)
        {
            await UpdateBooks(books.ToArray());
            await Cache.Update(books);
        }

        protected override async Task Save(IEnumerable<Author> authors)
        {
            await UpdateAuthors(authors.ToArray());
            await Cache.Update(authors);
        }

        public async Task<UploadToken> GetUploadToken(string filename)
        {
           return  await Post<UploadToken>("GetUploadToken", new GetUploadToken(filename));
        }


        class Progress : IProgress<Bytewizer.Backblaze.Models.ICopyProgress>
        {
            public void Report(Bytewizer.Backblaze.Models.ICopyProgress value)
            {

            }
        }

    }
}
