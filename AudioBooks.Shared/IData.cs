﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AudioBooks.Shared
{
    public abstract class IData
    {
        public abstract IQueryable<User> Users { get; }
        public abstract IQueryable<Book> Books { get; }
        public abstract IQueryable<Series> Series { get; }
        public abstract IQueryable<Author> Authors { get; }

        

        abstract protected Task Save(User user);
        abstract protected Task Save(IEnumerable<Series> series);
        abstract protected Task Save(IEnumerable<Book> books);
        abstract protected Task Save(IEnumerable<Author> authors);


        public async Task Update(User user)
        {
            var found = Users.FirstOrDefault(u => u.Username == user.Username);
            if(found != null)
            {
                found.Update(user);
            }
            else
            {
                found = user;
            }

            await Save(found);
        }

        public long GetUserBookProgress(User user, string bookid)
        {
            var book = user.GetBook(bookid);
            return book.Position;
        }

        public async Task UpdateUserBookProgress(User user, string bookid, long progress)
        {
            var userbook = user.GetBook(bookid);
            userbook.Position = progress;
            await Save(user);
        }

        public async Task UpdateUserBookFinished(User user, string bookid, bool finished)
        {
            var userbook = user.GetBook(bookid);
            userbook.ClientStatus |= ClientStatus.Finished;
            await Save(user);
        }


        public async Task Update(Book book)
        {
            await Update(new Book[] { book });
        }

        public async Task<Book[]> Update(IEnumerable<Book> books)
        {
            List<Book> ret = new List<Book>();

            var toupdatelookup = new HashSet<string>(books.Select(b => b.ID));

            var curbooks = Books.Where(b => toupdatelookup.Contains(b.ID)).ToDictionary(b => b.ID);

            foreach (var book in books)
            {
                Book found = null;

                //var found = Books.FirstOrDefault(b => b.ID == book.ID);
                if (curbooks.TryGetValue(book.ID, out found))
                {
                    found.Update(book);
                }
                else
                {
                    found = book;
                }
                
                ret.Add(found);
                
            }

            await Save(ret);
            return ret.ToArray();
        }

        public async Task<Author[]> Update(IEnumerable<Author> authors)
        {
            var ret = new List<Author>();
            foreach (var author in authors)
            {
                var found = GetAuthor(author.ID);
                if (found != null)
                {
                    found.Update(author);
                }
                else
                {
                    found = author;
                }

                ret.Add(found);

            }

            await Save(ret);
            return ret.ToArray();
        }

        public async Task Update(Series series)
        {
            await Update(new[] { series });
        }

        public async Task Update(IEnumerable<Series> serieses)
        {
            List<Series> ret = new List<Series>();
            foreach (var series in serieses)
            {
                var found = Series.FirstOrDefault(s => s.ID == series.ID);

                if (found == null)
                {
                    found = series;
                }
                else
                {
                    found.Update(series);
                }
                ret.Add(found);
            }
            await Save(ret);

        }

        public Book GetBook(string id)
        {
            return Books.FirstOrDefault(b => b.ID == id);
        }

        public IEnumerable<Book> GetSeriesBooks(Series series)
        {
            return series.Books.Select(b => GetBook(b));
        }

        public Author GetAuthor(string id)
        {
            return Authors.FirstOrDefault(a => a.ID == id);
        }

        public void AddToQueue(User user, string bookid)
        {
            user.Queue.Add(bookid);
        }
    }
}
