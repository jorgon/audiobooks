﻿namespace AudioBooks.Shared
{
    public class Series
    {
        public string Name { get; set; }
        public List<string> Books { get; set; } = new List<string>();
        public string ID { get; set; }

        public string Summary { get; set; }
        public bool IsPodcast { get; set; } = false;

        public void Update(Series series)
        {
            Name = series.Name;
            ID = series.ID;
            Summary = series.Summary;
            IsPodcast = series.IsPodcast;
            series.Books.Replace(Books);
        }
    }
}
