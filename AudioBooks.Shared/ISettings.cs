﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AudioBooks.Shared
{
    public interface ISettings
    {
        string Host { get; set; }
        string Token { get; set; }
        string FileDestination { get; set; }

        bool HasHost { get; }
        bool HasToken { get; }
        bool HasFileDestination { get; }

        void Reset();
    }
}
