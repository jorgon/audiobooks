﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AudioBooks.Shared
{
    public class BackblazeStorageSettings
    {
        public string KeyId { get; set; }
        public string AppKey { get; set; }
        public string Bucket { get; set; }

        string Filename = "Backblaze.json";

        public BackblazeStorageSettings()
        {
            if(File.Exists(Filename))
            {
                JsonConvert.PopulateObject(File.ReadAllText(Filename), this);
            }
            else
            {
                Save();
                Console.WriteLine("Backblaze settings not found: " + Path.Join(Environment.CurrentDirectory, Filename));
                System.Diagnostics.Process.GetCurrentProcess().Kill();
            }
        }

        public void Save()
        {
            var text = JsonConvert.SerializeObject(this);
            File.WriteAllText(Filename, text);
        }
    }
}
