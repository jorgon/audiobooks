﻿using Newtonsoft.Json;

namespace AudioBooks.Shared
{
    public class DataJson : IData
    {
        [JsonProperty("Users")]
        List<User> _Users { get; } = new List<User>();
        [JsonProperty("Books")]
        List<Book> _Books { get; } = new List<Book>();
        [JsonProperty("Series")]
        List<Series> _Series { get; } = new List<Series>();
        [JsonProperty("Authors")]
        List<Author> _Authors { get; } = new List<Author>();
        
        static string Filename = "Data.json";
        public static string Directory = null;

        static string FullFilename
        {
            get {
                if (Directory == null)
                    return Filename;
                return Path.Combine(Directory, Filename);
            }
        }

        public DataJson()
        {
            Load();
        }
        [JsonIgnore]
        public override IQueryable<User> Users => _Users.AsQueryable();
        [JsonIgnore]
        public override IQueryable<Book> Books => _Books.AsQueryable();
        [JsonIgnore]
        public override IQueryable<Series> Series => _Series.AsQueryable();
        [JsonIgnore]
        public override IQueryable<Author> Authors => _Authors.AsQueryable();

        public void Load()
        {
            if (File.Exists(FullFilename))
            {
                string content = File.ReadAllText(FullFilename);
                try
                {
                    JsonConvert.PopulateObject(content, this);
                }
                catch(Exception ex)
                {
                    Console.WriteLine("Error reading Data.json");
                    Console.WriteLine(ex.ToString());
                }
            }
        }


        Task _Save = null;

        public void Save()
        {
            if (_Save == null)
            {
                _Save = Task.Run(DoSave);
            }
            else
            {
                _Save.ContinueWith(state => DoSave());
            }
        }

        async Task DoSave()
        {
           // _Books.Sort((a, b) => a.ID.CompareTo(b.ID));
            //_Series.Sort((a, b) => a.ID.CompareTo(b.ID));
            var content = JsonConvert.SerializeObject(this, Formatting.Indented);
            await File.WriteAllTextAsync(FullFilename, content);
            Console.WriteLine($"Saved {this._Books.Count} books");
        }

        protected override async Task Save(User user)
        {
            if (!_Users.Contains(user))
            {
                _Users.Add(user);
            }
            Save();
        }

        protected override async Task Save(IEnumerable<Series> serieses)
        {
            foreach (var series in serieses)
            {
                if (!_Series.Contains(series))
                {
                    _Series.Add(series);
                }
            }
            Save();
        }

        protected override async Task Save(IEnumerable<Book> books)
        {
            foreach (var book in books)
            {
                if (GetBook(book.ID) == null)
                {
                    _Books.Add(book);
                }
            }
            Save();
        }

        protected override async Task Save(IEnumerable<Author> authors)
        {
            foreach (var author in authors)
            {
                if (GetAuthor(author.ID) == null)
                {
                    _Authors.Add(author);
                }
            }
            Save();
        }
    }

   
}
